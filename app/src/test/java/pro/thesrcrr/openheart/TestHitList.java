package pro.thesrcrr.openheart;

import com.github.javafaker.Faker;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import pro.thesrcrr.openheart.util.HitList;

import static junit.framework.TestCase.assertEquals;

public class TestHitList extends TestRunner {

    static Faker faker = new Faker();

    static class TestObject {
        int value;

        public TestObject(int value) {
            this.value = value;
        }

        public static TestObject fakeObj() {
            return new TestObject(faker.number().numberBetween(0, 100));
        }
    }

    @Test
    public void testListCreation() {
        HitList<TestObject> hitList = new HitList<TestObject>();
        ArrayList<TestObject> collection = new ArrayList<>();
        int sz = faker.number().numberBetween(1, 30);
        for (int i = 0; i < sz; ++i) {
            collection.add(new TestObject(i));
        }
        for (TestObject o : collection) {
            hitList.add(o);
        }
        // Ensure all counts are 1.
        for (TestObject o : collection) {
            assertEquals(1, hitList.get(o));
        }
    }

    @Test
    public void testListInvalidGet() {
        HitList<TestObject> hitList = new HitList<TestObject>();
        ArrayList<TestObject> collection = new ArrayList<>();
        int sz = faker.number().numberBetween(1, 30);
        for (int i = 0; i < sz; ++i) {
            collection.add(new TestObject(i));
        }
        for (TestObject o : collection) {
            hitList.add(o);
        }
        assertEquals(0, hitList.get(TestObject.fakeObj()));
    }

    @Test
    public void testListAdd() {
        HitList<TestObject> hitList = new HitList<TestObject>();

        TestObject obj1 = TestObject.fakeObj(), obj2 = TestObject.fakeObj(),
                   obj3 = TestObject.fakeObj(), obj4 = TestObject.fakeObj();
        hitList.add(obj1);

        hitList.add(obj2);
        hitList.add(obj2);
        hitList.add(obj2);
        hitList.add(obj2);

        hitList.add(obj3, 12);

        assertEquals(1, hitList.get(obj1));
        assertEquals(4, hitList.get(obj2));
        assertEquals(12, hitList.get(obj3));
        assertEquals(0, hitList.get(obj4));
    }

    @Test
    public void testRemove() {
        HitList<TestObject> hitList = new HitList<TestObject>();
        ArrayList<TestObject> collection = new ArrayList<>();
        int sz = faker.number().numberBetween(1, 30);

        for (int i = 0; i < sz; ++i) {
            collection.add(new TestObject(i));
        }


        int incAmount = 0;
        for (TestObject o : collection) {
            incAmount = faker.number().numberBetween(0, 10);
            for (int i = 0; i < incAmount; ++i) {
                hitList.add(o);
            }
        }

        TestObject toDecrement = collection.get(faker.number().numberBetween(0, sz));
        int startAmount = hitList.get(toDecrement);
        int decAmount = faker.number().numberBetween(0, startAmount-1);

        for (int i = 0; i < decAmount; ++i) {
            hitList.remove(toDecrement);
        }

        assertEquals(startAmount- decAmount, hitList.get(toDecrement));

        // If we remove it more times that what's there, no error is thrown

        for (int i = 0; i < startAmount; ++i) {
            hitList.remove(toDecrement);
        }
        assertEquals(0, hitList.get(toDecrement));
    }

    public void testHitListToJson() {
        Map<String, Integer> mapping = new HashMap<>();
        HitList<String> hitList = new HitList<>();
        for (int i = 0; i < 5; ++i)
            mapping.put(faker.lorem().word(), faker.number().randomDigitNotZero());
        for (String word : mapping.keySet())
            for (int i = 0; i < mapping.get(word); ++i)
                hitList.add(word);
    }
}
