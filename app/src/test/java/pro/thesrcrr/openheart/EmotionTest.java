package pro.thesrcrr.openheart;

import android.content.Context;
import android.util.Log;

import com.github.javafaker.Faker;

import org.json.JSONException;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import pro.thesrcrr.openheart.emote.Emotion;
import pro.thesrcrr.openheart.emote.EmotionList;
import pro.thesrcrr.openheart.emote.EmotionLoader;
import pro.thesrcrr.openheart.emote.EmotiveServiceEnvironment;
import pro.thesrcrr.openheart.emote.JsonColorMissingValueException;
import pro.thesrcrr.openheart.emote.JsonColorValueException;
import pro.thesrcrr.openheart.emote.MoodPlot;
import pro.thesrcrr.openheart.emote.OHLoadEnvironment;
import pro.thesrcrr.openheart.emote.PrimaryEmotion;
import pro.thesrcrr.openheart.emote.SecondaryEmotion;
import pro.thesrcrr.openheart.emote.TertiaryEmotion;
import pro.thesrcrr.openheart.emote.exc.MoodPlotException;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class EmotionTest extends TestRunner {

    private static final String TAG = "OpenHeart";
    private Context context;
    private Faker fake = new Faker();

    private Path getHere() {
        StackTraceElement ste = new Throwable().getStackTrace()[1];
        return FileSystems.getDefault().getPath("src",
                                                "main",
                                                "res",
                                                "raw",
                                                "default_emotions.csv").toAbsolutePath();
    }

    @Test
    public void testEmotionsLoaded() {
        EmotionLoader loader = new EmotionLoader(null);
        LinkedList<Emotion> emotions = new LinkedList<Emotion>();
        Path here = getHere();
        Log.d(TAG, "Loading from stream");
        try {
            FileInputStream f = new FileInputStream(here.toFile());
            emotions = loader.fromStream(f);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertTrue(emotions.size() > 0);
        TertiaryEmotion sampleTertiary = null;
        SecondaryEmotion sampleSecondary = null;
        PrimaryEmotion samplePrimary = null;
        String pName = "love", sName = "affection", tName = "attraction";
        for (Emotion e : emotions) {
//            System.err.println("samplePrimary = " + samplePrimary
//                               + ", sampleSecondary = " + sampleSecondary
//                               + ", sampleTertiary = " + sampleTertiary);
            samplePrimary = (e.getName().equals(pName) && (e instanceof PrimaryEmotion)) ? ((PrimaryEmotion) e) : samplePrimary;
            sampleSecondary = (e.getName().equals(sName) && (e instanceof SecondaryEmotion)) ? ((SecondaryEmotion) e) : sampleSecondary;
            sampleTertiary = (e.getName().equals(tName) && (e instanceof TertiaryEmotion)) ? ((TertiaryEmotion) e) : sampleTertiary;
//            Log.i(TAG, e.getName());
//            System.err.println("emotion: " + e.getName() + ", " + e.getClass().getName());
        }

        // Assert the relation between the primary & secondary relations
        assertNotNull(samplePrimary);
        assertNotNull(sampleSecondary);
        assertNotNull(sampleTertiary);

        assertNotNull(samplePrimary.getSecondaryEmotions());
        assertTrue(samplePrimary.getSecondaryEmotions().size() > 0);
        assertIn(sampleSecondary, samplePrimary.getSecondaryEmotions());
        assertEquals(sampleSecondary.getPrimaryEmotion(), samplePrimary);

        // Assert the relationship between the secondary & tertiary relationships
        assertNotNull(samplePrimary.getSecondaryEmotions());
        assertTrue(sampleSecondary.getTertiaryEmotions().size() > 0);
        assertIn(sampleTertiary, sampleSecondary.getTertiaryEmotions());
        assertEquals(sampleTertiary.getSecondaryEmotion(), sampleSecondary);
    }

    private EmotionList load() {
        EmotionLoader loader = new EmotionLoader(null);
        EmotionList emotions = new EmotionList();
        Path here = getHere();
        Log.d(TAG, "Loading from stream");
        try {
            FileInputStream f = new FileInputStream(here.toFile());
            emotions = loader.fromStream(f);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return emotions;
    }

    private PrimaryEmotion getRandomPrimaryEmotion() {
        EmotionList primaryEmotios = load().getPrimary();
        return (PrimaryEmotion) primaryEmotios.get(fake.number()
                                                       .numberBetween(0, primaryEmotios.size()));
    }

    private SecondaryEmotion getRandomSecondaryEmotion() {
        EmotionList secondaryEmotions = load().getSecondary();
        return (SecondaryEmotion) secondaryEmotions.get(fake.number()
                                                            .numberBetween(0,
                                                                           secondaryEmotions.size()));
    }

    private TertiaryEmotion getRandomTertiaryEmotion() {
        EmotionList tertiaryEmotios = load().getPrimary();
        return (TertiaryEmotion) tertiaryEmotios.get(fake.number()
                                                         .numberBetween(0, tertiaryEmotios.size()));
    }

    private Emotion getAnyEmotion () {
        EmotionList emotions = load();
        return load().get(fake.number().numberBetween(0, emotions.size()));
    }

    private EmotionList fakeEmotionSubset(int count) {
        EmotionList original = load();
        EmotionList emotions = new EmotionList();
        for (int i = 0; i < count; ++i) {
            emotions.push(original.get(fake.number().numberBetween(0, original.size())));
        }
        return emotions;
    }

    private EmotionList fakeEmotionSubset() {
        return fakeEmotionSubset(fake.number().numberBetween(0, load().size()));
    }

    private MoodPlot fakeMoodPlot(Date start, Date end, EmotionList emotions, int count) {
        MoodPlot plot = new MoodPlot(new EmotionList());
        for (int i = 0; i < count; ++i) {
            Date time = fake.date().between(start, end);
            Emotion emotion = emotions.get(fake.number().numberBetween(0, emotions.size()));
            plot.record(context, emotion, time);
        }
        return plot;
    }

    @Test
    public void testFlushAndLoad() throws JSONException, IOException, MoodPlotException, JsonColorValueException, ParseException, JsonColorMissingValueException {
        String path = "/tmp/EmotionTestOutput.json";
        Date start = fake.date().past(100, 30, TimeUnit.DAYS),
                end = fake.date().past(29, 1, TimeUnit.DAYS);
        MoodPlot plot = fakeMoodPlot(start, end, load().getTertiary(),
                                     fake.number().numberBetween(2, 10));
        String dumped = plot.toJson().toString();
        plot.setPath(path);
        plot.flush();
        OHLoadEnvironment env = new OHLoadEnvironment(load());
        EmotiveServiceEnvironment ese = new EmotiveServiceEnvironment(context);
        MoodPlot loaded = MoodPlot.fromJsonFile(path, env, ese);
        String loadedRaw = loaded.toJson().toString();
        assertEquals(dumped, loadedRaw);
        System.err.println("testFlushAndLoad: " + dumped);
        System.err.println("testFlushAndLoad: " + loadedRaw);
    }
}