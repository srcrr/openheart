package pro.thesrcrr.openheart;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import pro.thesrcrr.openheart.emote.EmotionList;
import pro.thesrcrr.openheart.emote.JsonColor;
import pro.thesrcrr.openheart.emote.JsonColorMissingValueException;
import pro.thesrcrr.openheart.emote.JsonColorValueException;
import pro.thesrcrr.openheart.emote.OHLoadEnvironment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class JsonColorTest {
    @Rule
    public final ExpectedException jsonColorValueException = ExpectedException.none();

    @Test
    public void testExportColorNoAlpha() {
        JSONObject c = null;
        int red = 119, green = 49, blue=2, alpha = 1;
        try {
            c = (new JsonColor(red, green, blue)).toJson();
            assertNotNull(c);
            System.err.println(c);
            assertEquals(red, c.getInt(JsonColor.RED));
            assertEquals(green, c.getInt(JsonColor.GREEN));
            assertEquals(blue, c.getInt(JsonColor.BLUE));
            assertEquals(alpha, c.getInt(JsonColor.ALPHA));
        } catch (JSONException | JsonColorValueException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testExportColorWithAlpha() {
        JSONObject c = null;
        int red = 119, green = 49, blue=2, alpha = 19;
        try {
            c = (new JsonColor(red, green, blue, alpha)).toJson();
            assertNotNull(c);
            System.err.println(c);
            assertEquals(red, c.getInt(JsonColor.RED));
            assertEquals(green, c.getInt(JsonColor.GREEN));
            assertEquals(blue, c.getInt(JsonColor.BLUE));
            assertEquals(alpha, c.getInt(JsonColor.ALPHA));
        } catch (JSONException | JsonColorValueException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testImportColorNoAlpha() {
        JSONObject c = new JSONObject();
        OHLoadEnvironment env = new OHLoadEnvironment(new EmotionList());
        try {
            c = c.put(JsonColor.RED, 32)
                    .put(JsonColor.GREEN, 133)
                    .put(JsonColor.BLUE, 83);
            assertNotNull(c);
            JsonColor color = JsonColor.fromJson(c, env);
            assertEquals(32, color.getRed());
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (JsonColorValueException e1) {
            e1.printStackTrace();
        } catch (JsonColorMissingValueException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testImportColorWithAlpha() {
        JSONObject c = new JSONObject();
        OHLoadEnvironment env = new OHLoadEnvironment(new EmotionList());
        try {
            c = c.put(JsonColor.RED, 32)
                    .put(JsonColor.GREEN, 133)
                    .put(JsonColor.BLUE, 83)
                    .put(JsonColor.ALPHA, 55);
            assertNotNull(c);
            JsonColor color = JsonColor.fromJson(c, env);
            assertEquals(32, color.getRed());
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (JsonColorValueException e1) {
            e1.printStackTrace();
        } catch (JsonColorMissingValueException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void testImportColorMissingValuesRed() {
        JSONObject c = new JSONObject();
        OHLoadEnvironment env = new OHLoadEnvironment(new EmotionList());
        try {
            c = c.put(JsonColor.GREEN, 32)
                    .put(JsonColor.BLUE, 83)
                    .put(JsonColor.ALPHA, 55);
            JsonColor.fromJson(c, env);
            fail("JsonColorMissingValueException not thrown for RED");
        } catch (JSONException | JsonColorValueException e) {
            fail();
        } catch (JsonColorMissingValueException e1) {
            assertEquals(e1.getField(), JsonColor.RED);
        }
    }


    @Test
    public void testImportColorMissingValuesGreen() {
        JSONObject c = new JSONObject();
        OHLoadEnvironment env = new OHLoadEnvironment(new EmotionList());
        try {
            c = c.put(JsonColor.RED, 32)
                    .put(JsonColor.BLUE, 83)
                    .put(JsonColor.ALPHA, 55);
            JsonColor.fromJson(c, env);
            fail("JsonColorMissingValueException not thrown for GREEN");
        } catch (JSONException | JsonColorValueException e) {
            fail();
        } catch (JsonColorMissingValueException e1) {
            assertEquals(e1.getField(), JsonColor.GREEN);
        }
    }


    @Test
    public void testImportColorMissingValuesBlue() {
        JSONObject c = new JSONObject();
        OHLoadEnvironment env = new OHLoadEnvironment(new EmotionList());
        try {
            c = c.put(JsonColor.RED, 32)
                    .put(JsonColor.GREEN, 83)
                    .put(JsonColor.ALPHA, 55);
            JsonColor.fromJson(c, env);
            fail("JsonColorMissingValueException not thrown for RED");
        } catch (JSONException | JsonColorValueException e) {
            fail();
        } catch (JsonColorMissingValueException e1) {
            assertEquals(e1.getField(), JsonColor.BLUE);
        }
    }


    @Test
    public void testValueValidationRedMin() {
        int v = -30;
        try {
            JsonColor c = new JsonColor(v, 13, 110);
            fail("Should have raised " + JsonColorValueException.class.getName()
                 + " on field " + JsonColor.RED);
        } catch (JsonColorValueException e) {
            assertEquals(v, e.getValue());
            assertEquals(JsonColor.RED, e.getField());
        }
    }

    @Test
    public void testValueValidationRedMax() {
        int v = 1024;
        try {
            JsonColor c = new JsonColor(v, 13, 110);
            fail("Should have raised " + JsonColorValueException.class.getName()
                 + " on field " + JsonColor.RED);
        } catch (JsonColorValueException e) {
            assertEquals(v, e.getValue());
            assertEquals(JsonColor.RED, e.getField());
        }
    }

    @Test
    public void testValueValidationGreenMin() {
        int v = -30;
        try {
            JsonColor c = new JsonColor(v, 13, 110);
            fail("Should have raised " + JsonColorValueException.class.getName()
                 + " on field " + JsonColor.GREEN);
        } catch (JsonColorValueException e) {
            assertEquals(v, e.getValue());
            assertEquals(JsonColor.RED, e.getField());
        }
    }

    @Test
    public void testValueValidationGreenMax() {
        int v = 1024;
        try {
            JsonColor c = new JsonColor(13, v, 110);
            fail("Should have raised " + JsonColorValueException.class.getName()
                 + " on field " + JsonColor.GREEN);
        } catch (JsonColorValueException e) {
            assertEquals(v, e.getValue());
            assertEquals(JsonColor.GREEN, e.getField());
        }
    }

    @Test
    public void testValueValidationBlueMin() {
        int v = -30;
        try {
            JsonColor c = new JsonColor(119, 13, v);
            fail("Should have raised " + JsonColorValueException.class.getName()
                    + " on field " + JsonColor.BLUE);
        } catch (JsonColorValueException e) {
            assertEquals(v, e.getValue());
            assertEquals(JsonColor.BLUE, e.getField());
        }
    }

    @Test
    public void testValueValidationBlueMax() {
        int v = 1024;
        try {
            JsonColor c = new JsonColor(13, 83, v);
            fail("Should have raised " + JsonColorValueException.class.getName()
                    + " on field " + JsonColor.BLUE);
        } catch (JsonColorValueException e) {
            assertEquals(v, e.getValue());
            assertEquals(JsonColor.BLUE, e.getField());
        }
    }
}
