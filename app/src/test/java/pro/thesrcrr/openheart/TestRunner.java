package pro.thesrcrr.openheart;

import androidx.test.runner.AndroidJUnitRunner;

import java.util.Collection;

import static junit.framework.TestCase.fail;

public class TestRunner extends AndroidJUnitRunner
{
    public final void assertIn(Object instance, Collection collection) {
        for (Object i : collection)
            if (i.equals(instance))
                return;
        fail(
                (new StringBuilder()).append(instance.toString())
                                     .append(" not found in ")
                                     .append(collection.toString()).toString()
        );
    }
}