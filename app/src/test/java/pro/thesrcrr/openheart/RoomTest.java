package pro.thesrcrr.openheart;

import com.github.javafaker.Faker;

import org.junit.Test;
import org.robolectric.annotation.Config;

import java.io.File;
import java.util.LinkedList;
import java.util.regex.Pattern;

import pro.thesrcrr.openheart.emote.data.FileHistory;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.fail;

public class RoomTest extends TestRunner {

    private Faker fake = new Faker();
    private final static String PATH_REGEX = "[A-Za-z]";
    private final static String EXTENSION_REGEX = Pattern.compile("[a-z]{2,3}").toString();

    // NOTE: Ensure run configuration current dir is set to `$MODULE_WORKING_DIR$`
    private final static String MANIFEST_PATH = "src/main/AndroidManifest.xml";

    private String fakePath(int depth, String extension) {
        StringBuilder builder = new StringBuilder();
        for (int i = 1; i < depth; ++i) {
            builder.append(fake.regexify(PATH_REGEX))
                   .append(File.pathSeparator);
        }
        builder.append(fake.regexify(PATH_REGEX));
        if (extension != null) {
            builder.append(".").append(extension);
        }
        return builder.toString();
    }

    private String fakePath(int depth) {
        return fakePath(depth, null);
    }

    private String fakePath() {
        return fakePath(fake.number().numberBetween(1, 10));
    }

    private String fakeFile(int depth) {
        return fakePath(depth, fake.regexify(EXTENSION_REGEX));
    }

    private String fakeFile() {
        return fakeFile(fake.number().numberBetween(1, 30));
    }


    @Test
    @Config(manifest = MANIFEST_PATH)
    public void testInsertIntoHistory() throws Exception {
        String path = fakeFile();
        FileHistory history = FileHistory.getInstance(getContext());
        history.insert(path);
        assertEquals(1, history.size());
    }


    @Test
    @Config(manifest = MANIFEST_PATH)
    public void testReorderHistory() throws Exception {
        LinkedList<String> paths = new LinkedList<>();
        for (int i = 0; i < fake.number().numberBetween(2, 30); ++i) {
            paths.push(fakePath());
        }
        String toReinsert = paths.get(fake.number().numberBetween(1, paths.size() - 1));
        FileHistory history = FileHistory.getInstance(getContext());
        history.insert(toReinsert);

        // Verify that the path is the first one and is not anywhere else in the list.
        // TODO: Make history a Set
        assertNotNull(history);
        assertEquals(history.get(0), toReinsert);
        for (int i = 2; i < history.size(); ++i) {
            if (history.get(i).equals(toReinsert)) {
                fail(
                        (new StringBuilder().append("Found element '")
                                .append(toReinsert)
                                .append("' at index ")
                                .append(i)
                        ).toString()
                );
            }
        }
    }


    @Test
    @Config(manifest = MANIFEST_PATH)
    public void testHistoryNoItems() {
        FileHistory view = FileHistory.getInstance(getContext());
        assertEquals(view.size(), 0);
    }
}
