package pro.thesrcrr.openheart;

import android.content.Context;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import com.github.javafaker.Faker;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.text.ParseException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import pro.thesrcrr.openheart.emote.Emotion;
import pro.thesrcrr.openheart.emote.EmotionList;
import pro.thesrcrr.openheart.emote.EmotionLoader;
import pro.thesrcrr.openheart.emote.EmotiveServiceEnvironment;
import pro.thesrcrr.openheart.emote.JsonColorMissingValueException;
import pro.thesrcrr.openheart.emote.JsonColorValueException;
import pro.thesrcrr.openheart.emote.MoodPlot;
import pro.thesrcrr.openheart.emote.OHLoadEnvironment;
import pro.thesrcrr.openheart.util.HitList;

import static junit.framework.TestCase.assertEquals;

@RunWith(AndroidJUnit4.class)
public class TestMoodChart {

    static Faker faker = new Faker();
    private static Random r = new Random();

    private Context context;
    private MoodPlot moodPlot;
    private EmotionList defaultEmotions;

    public TestMoodChart() throws IOException {
        super();
        this.context = InstrumentationRegistry.getInstrumentation().getContext();
        EmotionLoader loader = new EmotionLoader(this.context);
        defaultEmotions = loader.fromResource(R.raw.default_emotions);
        moodPlot = new MoodPlot(defaultEmotions);
    }

    private Emotion randomEmotion() {
        return defaultEmotions.get(r.nextInt(defaultEmotions.size()));
    }

    @Test
    public void testEmotionHitListRecording() throws JSONException, JsonColorValueException, ParseException, JsonColorMissingValueException {
        Emotion randomEm = randomEmotion();
        for (int i = 0; i < 5; ++i) {
            moodPlot.record(this.context, randomEm, faker.date().past(5, TimeUnit.DAYS));
        }
        MoodPlot expected = moodPlot;
        JSONObject expectedJson = moodPlot.toJson();

        OHLoadEnvironment loadEnv = new OHLoadEnvironment(defaultEmotions);
        EmotiveServiceEnvironment serviceEnv = new EmotiveServiceEnvironment(this.context);
        MoodPlot result = MoodPlot.fromJson(expectedJson, loadEnv, serviceEnv);

        HitList<Emotion> expectedCounts = expected.getCounts();
        HitList<Emotion> resultCounts = result.getCounts();

        assertEquals(expectedCounts, resultCounts);
    }
}
