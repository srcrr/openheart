package pro.thesrcrr.openheart;

import android.Manifest;
import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.GrantPermissionRule;

import com.github.javafaker.Faker;

import org.apache.commons.collections4.list.TreeList;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import pro.thesrcrr.openheart.calendar.UserCalendar;
import pro.thesrcrr.openheart.emote.Emotion;
import pro.thesrcrr.openheart.emote.EmotionList;
import pro.thesrcrr.openheart.emote.EmotionLoader;
import pro.thesrcrr.openheart.emote.MoodPlot;
import pro.thesrcrr.openheart.trigger.TriggerMetric;
import pro.thesrcrr.openheart.util.HitList;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

//import android.support.test.InstrumentationRegistry;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class AndroidMainActivityTest {

    @Rule
    public GrantPermissionRule permissionRule = GrantPermissionRule.grant(Manifest.permission.WRITE_CALENDAR,
            Manifest.permission.READ_CALENDAR);
    private static final String TAG = "TestMainActivity";
    private Instrumentation.ActivityMonitor monitor;
    private Instrumentation instrumentation;
    private Activity currentActivity;
    private final Faker faker = new Faker();

    private final static String CALENDAR_NAME = "Simple Offline Calendar";

    UserCalendar calendar;

    private final static int DAYS = 5, EVENT_COUNT_MAX = 15;
    private EmotionList emotions;

    public void createCalendar(Context context) {

        calendar = UserCalendar.create(context,
                                        "instrumentation_calendar_test",
                                "Instrumentation Calendar Test",
                                        "Test Account",
                                        "Test Owner");

        if (calendar == null) {
            throw new NullPointerException("Could not find calendar '" + CALENDAR_NAME + "'."
                + " Download the `Offline Calendar` application to android to create "
                + " the calendar and try again.");
        }

        Log.d(TAG, "setUp: Created calendar '" + calendar.getDisplayName() + "'");
    }

    public void addEvents() {

        int count = faker.number().numberBetween(4, 19);
        int allDay = faker.number().numberBetween(1, 2);
        int intervals = 0;
        String title = null, location = null;
        Date start = null, end = null;

        Log.d(TAG, "setUp: Generating " + count + " events.");

        for (int i = 0; i < count; ++i) {
            title = faker.lorem().sentence();
            location = faker.lordOfTheRings().location();
            Log.d(TAG, "setUp: Create event " + title + " @ " + location);
            start = faker.date().past(10, TimeUnit.DAYS); // 10 days ago
            if (allDay == 0) {
                intervals = faker.number().numberBetween(1, 10);
                long endLong = (long) (intervals * 15 * 60);
                end = Date.from(start.toInstant().plusSeconds(endLong));
                calendar.createEvent(title, location, start.getTime(),
                        end.getTime());
            } else {
                calendar.createEvent(title, location, start.getTime());
            }
        }
    }

    public void registerPermissions() {
    }

    public void addMonitor() {

        monitor = instrumentation.addMonitor(MainActivity.class.getName(),
                null, false);
    }

    public void startActivity() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setClassName(instrumentation.getTargetContext(), MainActivity.class.getName());
        instrumentation.startActivitySync(intent);

        currentActivity = instrumentation.waitForMonitor(monitor);
        assertNotNull(currentActivity);
    }

    @Before
    public void grantPhonePermission() {
        // In M+, trying to call a number will trigger a runtime dialog. Make sure
        // the permission is granted before running this test.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getInstrumentation().getUiAutomation().executeShellCommand(
                    "pm grant " + instrumentation.getTargetContext().getPackageName()
                            + " android.permission.READ_CALENDAR");
            getInstrumentation().getUiAutomation().executeShellCommand(
                    "pm grant " + instrumentation.getTargetContext().getPackageName()
                            + " android.permission.WRITE_CALENDAR");
        }
    }

    public void loadEmotions() {
        EmotionLoader loader = new EmotionLoader(instrumentation.getTargetContext());
        try {
            emotions = loader.fromResource(R.raw.default_emotions);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void enableAllTriggerMetrics () {
        Context context = instrumentation.getTargetContext();
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        final Resources res = context.getResources();

        final String keyTriggerMetrics = res.getString(R.string.preference_key_trigger_metrics);
        final String[] metricValuesArray = res.getStringArray(R.array.mod_trigger_metric_values);
        final String keyTimeframeAfter = res.getString(R.string.preference_key_trigger_timeframe_after);

        // Start editing
        SharedPreferences.Editor editor = pref.edit();
        TreeSet<String> metrics = new TreeSet<>(Arrays.asList(metricValuesArray));
        editor.putStringSet(keyTriggerMetrics, metrics);

        editor.putString(keyTimeframeAfter, "24.0");

        // Finish editing
        editor.commit();
    }

    @Before
    public void setUp() {
        Log.d(TAG, "setUp: PERFORMING SETUP");
        instrumentation = getInstrumentation();

        loadEmotions();
        enableAllTriggerMetrics();
        addMonitor();
        registerPermissions();

        startActivity();

//        Context context = InstrumentationRegistry.getTargetContext();

        Context context = instrumentation.getTargetContext();

        UserCalendar.requestReadPermissions(currentActivity);

        createCalendar(context);
        addEvents();
    }

    @After
    public void tearDown() {
        Log.d(TAG, "tearDown: Clearing calendar '" + calendar.getDisplayName() + "'");

        calendar.clear();

        if (monitor != null)
            instrumentation.removeMonitor(monitor);
    }

    @Test
    public void testGetUserCalendars() {
        Context context = instrumentation.getTargetContext();
        Log.d(TAG, "setUp: Calendar = " + calendar.getDisplayName());

        Date now = Calendar.getInstance().getTime();
        Date start = Date.from(now.toInstant().minusSeconds(DAYS * 60 * 60 * 24));

        UserCalendar.requestReadPermissions(currentActivity);

        TreeList<UserCalendar> calendars = UserCalendar.getAll(context);

        assertNotNull(calendars);
    }

    public void prefillLastDay() {
        Date time = Date.from(Calendar.getInstance().getTime().toInstant().minus(Duration.ofHours(24)));
        String title = null, location = null;
        Date startTime = null, endTime = null;
        for (int i = 0; i < 24; ++i) {
            title = faker.friends().quote();
            location = faker.starTrek().location();
            startTime = endTime == null ? time : startTime;
            endTime = Date.from(startTime.toInstant().plus(Duration.ofHours(1)));
            calendar.createEvent(title, location, startTime.getTime(), endTime.getTime());
        }
    }

    @Test
    public void testMoodTrigger() {
        MainActivity mainActivity = (MainActivity) currentActivity;

        Date recordTime = faker.date().past(24, TimeUnit.HOURS);
        Emotion emotion = emotions.get(faker.number().numberBetween(1, emotions.size()));

        Log.d(TAG, "testMoodTrigger: Recording emotion `" + emotion.getName() + "`");

        MoodPlot moodPlot = new MoodPlot(emotions);

        moodPlot.record(instrumentation.getTargetContext(),
                emotion,
                recordTime
        );

        HashMap<Date, TriggerMetric> triggerMetrics = moodPlot.getTriggerMetrics();

        for (Date key : triggerMetrics.keySet()) {
            Log.d(TAG, "testMoodTrigger: " + key.toString()
                    + " => " + triggerMetrics.get(key).toString());
        }

        assertTrue(triggerMetrics.size() > 0);
        assertTrue(triggerMetrics.containsKey(recordTime));

        // Also verify that the trigger is recorded for the mood.
        TreeList<TriggerMetric> metricsForMood = moodPlot.getMetricsForMood(emotion);
        assertTrue(metricsForMood.contains(triggerMetrics.get(recordTime)));

        HashMap<Emotion, TreeList<TriggerMetric>> mapping = moodPlot.getEmotionTriggerMapping();
        assertTrue(mapping.containsKey(emotion));
    }

    @Test
    public void testHitList() {
        MoodPlot moodPlot = new MoodPlot(emotions);
        Context context = instrumentation.getTargetContext();
        final int slotSize = 8;
        ArrayList<Emotion> slots = new ArrayList<>(slotSize);
        Emotion emotion = emotions.get(faker.number().numberBetween(0, emotions.size()));
        for (int i = 0; i < slotSize; ++i) {
            while (slots.contains(emotion))
                emotion = emotions.get(faker.number().numberBetween(0, emotions.size()));
            slots.add(i, emotion);
            Log.d(TAG, "testHitList: " + emotion.getName() + " => " + i);
        }

        for (int i = 0; i < slotSize; ++i) {
            for (int j = 0; j < i; ++j) {
                Log.i(TAG, "testHitList: RECORD [" + slots.get(i).getName() + "]");
                Log.i(TAG, "testHitList: hitList=" + moodPlot.getCounts().size());
                moodPlot.record(context, slots.get(i));
            }
        }

        HitList<Emotion> hitList = moodPlot.getCounts();
        for (int i = 0; i < slotSize; ++i) {
            emotion = slots.get(i);
            Log.d(TAG, "testHitList: " + emotion.getName() + " " + i + " // " + hitList.get(emotion));
            assertEquals(i, hitList.get(emotion));
        }
    }


    @Test
    public void testHitListToJson() throws JSONException {
        MoodPlot moodPlot = new MoodPlot(emotions);
        Context context = instrumentation.getTargetContext();
        final int slotSize = 8;
        ArrayList<Emotion> slots = new ArrayList<>(slotSize);
        Emotion emotion = emotions.get(faker.number().numberBetween(0, emotions.size()));
        for (int i = 0; i < slotSize; ++i) {
            while (slots.contains(emotion))
                emotion = emotions.get(faker.number().numberBetween(0, emotions.size()));
            slots.add(i, emotion);
            Log.d(TAG, "testHitList: " + emotion.getName() + " => " + i);
        }

        for (int i = 0; i < slotSize; ++i) {
            for (int j = 0; j < i; ++j) {
                Log.i(TAG, "testHitList: RECORD [" + slots.get(i).getName() + "]");
                Log.i(TAG, "testHitList: hitList=" + moodPlot.getCounts().size());
                moodPlot.record(context, slots.get(i));
            }
        }

        JSONObject jMoodPlot = moodPlot.toJson();
        JSONObject jcounts = jMoodPlot.getJSONObject(MoodPlot.COUNTS);
        Log.d(TAG, "testHitListToJson: jcounts = " + jcounts);
        for (int i = 1; i < slots.size(); ++i) {
            Emotion e = slots.get(i);
            int count = jcounts.getInt(String.valueOf(e.getId()));
            assertEquals(count, i);
        }
    }
}
