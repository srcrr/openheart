package pro.thesrcrr.openheart.trigger;

import android.content.Context;

import org.apache.commons.collections4.list.TreeList;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;

import pro.thesrcrr.openheart.calendar.UserCalendar;
import pro.thesrcrr.openheart.emote.EmotiveServiceEnvironment;
import pro.thesrcrr.openheart.exceptions.TriggerMetricException;

public class TriggerMetricFactory {
    protected final static String CLASS = "class";
    private TreeList<UserCalendar> calendars;
    private Context context;

    public TriggerMetricFactory(Context context) {
        this.context = context;
    }

    public static TriggerMetric fromJson(JSONObject jsonObject, EmotiveServiceEnvironment env) throws JSONException, TriggerMetricException, ParseException {
        String className = jsonObject.getString(CLASS);

        // Pass the JsonObject to the appropriate TriggerMetric class.

        // Calendar Event
        if (className.equals(CalendarEventMetric.class.getName())) {
            return (TriggerMetric) CalendarEventMetric.fromJson(jsonObject, env);
        } else if (className.equals(TimeOfDayMetric.class.getName())) {
            return (TimeOfDayMetric) TimeOfDayMetric.fromJson(jsonObject, env);
        }

        // If a `class` key is not given or it's invalid, thow an error.
        String error = new StringBuilder("Could not parse TriggerMetric. No such class ")
                            .append(className)
                            .append(" found")
                            .toString();

        throw new TriggerMetricException(error);
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public TreeList<UserCalendar> getCalendars() {
        return calendars;
    }

    public void setCalendars(TreeList<UserCalendar> calendars) {
        this.calendars = calendars;
    }
}
