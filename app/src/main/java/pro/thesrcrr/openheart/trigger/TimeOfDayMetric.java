package pro.thesrcrr.openheart.trigger;

import android.os.Build;

import androidx.annotation.RequiresApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.time.Instant;
import java.util.Date;

import pro.thesrcrr.openheart.emote.EmotiveServiceEnvironment;

public class TimeOfDayMetric extends TriggerMetric {
    public TimeOfDayMetric(Date date) {
        super(date);
    }

    @Override
    public JSONObject toJson() throws JSONException {
        return super.toJson();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static TimeOfDayMetric fromJson(JSONObject jsonObject, EmotiveServiceEnvironment env) throws JSONException {
        return new TimeOfDayMetric(
                Date.from(Instant.ofEpochMilli(jsonObject.getLong(KEY_DATE)))
        );
    }
}
