package pro.thesrcrr.openheart.trigger;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public abstract class TriggerMetric {

    protected Date date;

    protected final static String KEY_DATE = "date",
                                  KEY_CLASS = "class";

    public TriggerMetric(Date date) {
        this.date = date;
    }

    public JSONObject toJson() throws JSONException {
        return new JSONObject()
                    .put(KEY_DATE, date.getTime())
                    .put(KEY_CLASS, getClass().getName());
    }

    public Date getDate() {
        return date;
    }
}
