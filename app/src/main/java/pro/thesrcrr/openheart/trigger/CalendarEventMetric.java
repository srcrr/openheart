package pro.thesrcrr.openheart.trigger;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Date;

import pro.thesrcrr.openheart.calendar.UserCalendarEvent;
import pro.thesrcrr.openheart.emote.EmotiveServiceEnvironment;
import pro.thesrcrr.openheart.emote.MoodPlot;

public class CalendarEventMetric extends TriggerMetric {

    private static final String KEY_EVENT = "event";

    public UserCalendarEvent getUserCalendarEvent() {
        return userCalendarEvent;
    }

    public void setUserCalendarEvent(UserCalendarEvent userCalendarEvent) {
        this.userCalendarEvent = userCalendarEvent;
    }

    private UserCalendarEvent userCalendarEvent;

    public CalendarEventMetric(Date date, UserCalendarEvent userCalendarEvent) {
        super(date);
        this.userCalendarEvent = userCalendarEvent;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        return super.toJson()
                   .put(KEY_EVENT, userCalendarEvent.toJsonReference());
    }

    public static CalendarEventMetric fromJson(JSONObject jsonObject, EmotiveServiceEnvironment env) throws JSONException, ParseException {
        UserCalendarEvent event = UserCalendarEvent.fromJson(env.getUserCalendars(),
                                                             jsonObject.getJSONObject(KEY_EVENT));
        Date date = MoodPlot.keyToDate(jsonObject.getString(KEY_DATE));
        return new CalendarEventMetric(date, event);
    }

    @NonNull
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("<Metric:Calendar (date=<")
                .append(date.toString())
                .append(">, userCalendarEvent=")
                .append(userCalendarEvent.toString())
                .append(")>")
                ;
        return builder.toString();
    }
}
