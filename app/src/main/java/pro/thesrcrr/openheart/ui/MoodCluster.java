package pro.thesrcrr.openheart.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import androidx.appcompat.widget.LinearLayoutCompat;

import java.io.IOException;

import pro.thesrcrr.openheart.emote.Emotion;
import pro.thesrcrr.openheart.emote.EmotionList;
import pro.thesrcrr.openheart.emote.EmotionLoader;
import pro.thesrcrr.openheart.emote.PrimaryEmotion;
import pro.thesrcrr.openheart.emote.SecondaryEmotion;
import pro.thesrcrr.openheart.emote.TertiaryEmotion;

public class MoodCluster extends LinearLayoutCompat {

    private static final String TAG = "MoodCluster";
    private static final Class<? extends Emotion> DEFAULT_FILTER = PrimaryEmotion.class;
    private EmotionList moods;
    Class<? extends Emotion> filter;
    private OnEmotionSelectedListener onEmotionSelectedListener;

    public interface OnEmotionSelectedListener {
        void onPrimaryEmotionSelected(MoodCluster moodCluster, PrimaryEmotion emotion);
        void onSecondaryEmotionSelected(MoodCluster moodCluster, SecondaryEmotion emotion);
        void onTertiaryEmotionSelected(MoodCluster moodCluster, TertiaryEmotion emotion);
    }


    public MoodCluster(Context context, AttributeSet attrs) {
        super(context, attrs);
        moods = new EmotionList();
        this.setOrientation(LinearLayoutCompat.VERTICAL);
    }

    public MoodCluster(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        moods = new EmotionList();
        this.setOrientation(LinearLayoutCompat.VERTICAL);
    }

    public MoodCluster(Context context, EmotionList moods) {
        super(context);
        this.moods = moods;
        this.filter = PrimaryEmotion.class;
        this.setOrientation(LinearLayoutCompat.VERTICAL);
    }

    public void setMoods(EmotionList moods) {
        this.moods = moods;
        return;
    }

    public void removeButtons() {
        removeAllViews();
    }

    public MoodCluster(Context context, int resourceId) {
        super(context);
        EmotionLoader loader = new EmotionLoader(context);
        EmotionList moods = null;
        try {
            moods = loader.fromResource(resourceId);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.moods = moods;
        this.filter = PrimaryEmotion.class;
    }

    public void createMoodButtons() {

        Log.d(TAG, "createMoodButtons: " + moods.size() + " emotions");
        
        for (Emotion mood : moods) {
            MoodButton buttonView = new MoodButton(getContext(), mood, this);
            Log.d(TAG, "createMoodButtons: Adding " + mood + " to view");
            this.addView(buttonView);
        }
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        Log.d(TAG, "Layout changed: left=" + left + ", top=" + top + ", right=" + right
              + ", bottom=" + bottom);
        final int count = getChildCount();
        int curWidth, curHeight, curLeft, curTop, maxHeight;

        //get the available size of child view
        final int childLeft = this.getPaddingLeft();
        final int childTop = this.getPaddingTop();

        final int childRight = this.getMeasuredWidth() - this.getPaddingRight();
        final int childBottom = this.getMeasuredHeight() - this.getPaddingBottom();

        final int childWidth = childRight - childLeft;
        final int childHeight = childBottom - childTop;

        maxHeight = 0;
        curLeft = childLeft;
        curTop = childTop;
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            if (child.getVisibility() == GONE)
                return;

            //Get the maximum size of the child
            child.measure(MeasureSpec.makeMeasureSpec(childWidth, MeasureSpec.AT_MOST), MeasureSpec.makeMeasureSpec(childHeight, MeasureSpec.AT_MOST));
            curWidth = child.getMeasuredWidth();
            curHeight = child.getMeasuredHeight();
            //wrap is reach to the end
            if (curLeft + curWidth >= childRight) {
                curLeft = childLeft;
                curTop += maxHeight;
                maxHeight = 0;
            }
            //do the layout
            child.layout(curLeft, curTop, curLeft + curWidth, curTop + curHeight);
            //store the max height
            if (maxHeight < curHeight)
                maxHeight = curHeight;
            curLeft += curWidth;
        }
    }
    
    public void setOnEmotionSelectedListener(OnEmotionSelectedListener listener) {
        onEmotionSelectedListener = listener;
    }

    public void onEmotionSelected(Emotion emotion) {
        if (onEmotionSelectedListener == null)
            return;
        if (emotion instanceof PrimaryEmotion) {
            onEmotionSelectedListener.onPrimaryEmotionSelected(this, (PrimaryEmotion) emotion);
        } else if (emotion instanceof SecondaryEmotion) {
            onEmotionSelectedListener.onSecondaryEmotionSelected(this, (SecondaryEmotion) emotion);
        } else {
            onEmotionSelectedListener.onTertiaryEmotionSelected(this, (TertiaryEmotion) emotion);
        }
    }
}
