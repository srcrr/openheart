package pro.thesrcrr.openheart.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.widget.LinearLayoutCompat;

import pro.thesrcrr.openheart.emote.Emotion;

@SuppressLint("AppCompatCustomView")
public class MoodButton extends Button {

    private static final String TAG = "MoodButton";
    private Emotion emotion;
    private final OnAttachStateChangeListener onAttachStateListener = new OnAttachStateChangeListener() {
        private String TAG = "EmotionButton";

        @Override
        public void onViewAttachedToWindow(View v) {
            View v2 = (View) v.getParent();
            Log.i(TAG , "view = " + v2.getWidth());
        }

        @Override
        public void onViewDetachedFromWindow(View v) {

        }
    };

    public class ClusterButtonClickListener implements OnClickListener {

        private MoodCluster cluster;

        public ClusterButtonClickListener(MoodCluster cluster) {
            this.cluster = cluster;
        }

        public MoodCluster getCluster() {
            return cluster;
        }

        public void setCluster(MoodCluster cluster) {
            this.cluster = cluster;
        }

        @Override
        public void onClick(View v) {
            MoodButton button = (MoodButton) v;
            Emotion emotion = button.getEmotion();
            Log.d(TAG, "onClick: " + button.getClass().getName() + " " + emotion.getName() );
            this.cluster.onEmotionSelected (emotion);
        }
    }

    public MoodButton(Context context, Emotion emotion, MoodCluster cluster) {
        super(context);
        this.emotion = emotion;
        this.addOnAttachStateChangeListener(onAttachStateListener);
        this.setLayoutParams(new LinearLayoutCompat.LayoutParams(LinearLayoutCompat.LayoutParams.MATCH_PARENT, LinearLayoutCompat.LayoutParams.MATCH_PARENT));
        this.setText(this.emotion.getNameAsTitle());
        this.setOnClickListener(new ClusterButtonClickListener(cluster));
    }

    public Emotion getEmotion() {
        return emotion;
    }

    public void setEmotion(Emotion emotion) {
        this.emotion = emotion;
    }
}
