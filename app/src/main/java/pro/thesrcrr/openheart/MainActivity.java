package pro.thesrcrr.openheart;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;

import pro.thesrcrr.openheart.charts.SimpleChart;
import pro.thesrcrr.openheart.emote.INotifyMoodplotChanged;
import pro.thesrcrr.openheart.emote.JsonColorMissingValueException;
import pro.thesrcrr.openheart.emote.JsonColorValueException;
import pro.thesrcrr.openheart.emote.MoodPlot;
import pro.thesrcrr.openheart.emote.OpenHeartMoodRecorder;
import pro.thesrcrr.openheart.emote.data.FileHistory;
import pro.thesrcrr.openheart.emote.exc.MoodPlotException;
import pro.thesrcrr.openheart.fragments.MoodClustersFragment;
import pro.thesrcrr.openheart.fragments.OpenHeartFragmentNotFoundException;
import pro.thesrcrr.openheart.fragments.SettingsFragment;
import pro.thesrcrr.openheart.fragments.TriggerListFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "OpenHeart";
    private static final String FRAGMENT_TAG_MOOD_CLUSTERS = "fragment_mood_clusters",
                                FRAGMENT_TAG_CHARTS_SIMPLE = "fragment_charts_simple",
                                FRAGMENT_TAG_PREFERENCES = "fragment_preferences",
                                FRAGMENT_TAG_TRIGGER_LIST = "fragment_trigger_list";
    private static final int FRAME_LAYOUT_ID = R.id.frame_openheart_fragments;
    private static final String CHANNEL_ID = "pro.thesrcrr.openheart.OpenHeart";
    private static final int REQUEST_CODE_MOOD_PLOT_SELECTED = 1,
                             REQUEST_CODE_MOOD_PLOT_CREATED = 2;
    //    private static final int CHANNEL_ID = 993;
    private OpenHeartMoodRecorder openHeartService;
    private FileHistory fileHistoryModel;

    private static final int MY_PERMISSION = 1;
    private final static String [] permissions = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };

    // Service variables
    private ServiceConnection connection = new ServiceConnection() {

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onServiceConnected(ComponentName name, IBinder service2) {
            OpenHeartMoodRecorder.MoodRecorderBinder binder = (OpenHeartMoodRecorder.MoodRecorderBinder) service2;
            setOpenHeartService(binder.getService());
            setBound(true);
            openHeartService.loadMoodPlot();
        }


        @Override
        public void onServiceDisconnected(ComponentName name) {
            setBound(false);
        }
    };
    private boolean isBound = false;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @androidx.annotation.RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // housekeeping
        requestPermissions();

        addFragments();

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_mood_selector);

//        recorder = new OpenHeartMoodRecorder();
//
//        Intent intent = new Intent(this, OpenHeartMoodRecorder.class);
//        startService(intent);

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onStart() {
        super.onStart();
        startService();
    }

    private void startService() {
        Intent intent = new Intent(this, OpenHeartMoodRecorder.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(connection);
        isBound = false;
    }

    public ServiceConnection getConnection() {
        return connection;
    }

    public void setConnection(ServiceConnection connection) {
        this.connection = connection;
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            navigateToFragment(FRAGMENT_TAG_MOOD_CLUSTERS);
        }
        closeDrawer();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void addFragments() {

        Log.d(TAG, "addFragments: ");

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        // -- Mood clusters --
        MoodClustersFragment moodClustersFragment = new MoodClustersFragment(this);
        fragmentTransaction.add(FRAME_LAYOUT_ID, moodClustersFragment,
                                FRAGMENT_TAG_MOOD_CLUSTERS);
        fragmentTransaction.addToBackStack(null);

        // -- Triggers --
        TriggerListFragment triggerListFragment = new TriggerListFragment(this);
        fragmentTransaction.add(FRAME_LAYOUT_ID, triggerListFragment,
                                FRAGMENT_TAG_TRIGGER_LIST);
        fragmentTransaction.addToBackStack(null);

        // -- Simple chart --
        SimpleChart simpleChart = new SimpleChart(this);
        fragmentTransaction.add(FRAME_LAYOUT_ID, simpleChart,
                                FRAGMENT_TAG_CHARTS_SIMPLE);
        fragmentTransaction.addToBackStack(null);

        // -- Preferences --
        SettingsFragment settingsFragment = new SettingsFragment();
        fragmentTransaction.add(FRAME_LAYOUT_ID, settingsFragment,
                                FRAGMENT_TAG_PREFERENCES);
        fragmentTransaction.addToBackStack(null);

        // commit everything
        fragmentTransaction.commit();

        Log.d(TAG, "addFragments: " + fragmentManager.getFragments().size() + " fragments");
    }

    private void requestPermissions () {

        ActivityCompat.requestPermissions(this, permissions, MY_PERMISSION);

        for (String p : permissions) {
            if (ContextCompat.checkSelfPermission(this, p)
                    != PackageManager.PERMISSION_GRANTED) {
                Log.w(TAG, "requestPermissions: Permission " + p + " not granted" );
            }
        }
    }

//    private void serviceSetup() {
//        serviceSetupLoadDefaultMoods();
//    }
//
//    private void serviceSetupLoadDefaultMoods() {
//        Log.d(TAG, "serviceSetupLoadDefaultMoods: Requesting service load default moods");
//        Intent intent = new Intent(this, OpenHeartMoodRecorder.class);
//        intent.setAction(ACTION_LOAD_DEFAULT_MOODS);
//        startService(intent);
//    }

    private void showErrorNotification(String message, Exception e) {
        NotificationCompat.Builder builder;
        builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        builder.setSmallIcon(android.R.drawable.ic_dialog_alert);
        builder.setContentTitle("OpenHeart Error");
        builder.setContentText(message);
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public boolean navigateToFragment(String fragmentTag) {

//        loadMoodPlot();

        // TODO: custom fragment manager to be able to notify on moodplot change.
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        Fragment fragment = manager.findFragmentByTag(fragmentTag);

        if (fragment == null) {
            try {
                throw new OpenHeartFragmentNotFoundException(fragmentTag);
            } catch (OpenHeartFragmentNotFoundException e) {
                e.printStackTrace();
            }
            return false;
        }

        Log.d(TAG, "Switching to fragment: " + fragmentTag);
        transaction.replace(FRAME_LAYOUT_ID, fragment);
        transaction.addToBackStack(null).commit();

        return true;
    }

    private void closeDrawer() {
        ((DrawerLayout) findViewById(R.id.drawer_layout)).closeDrawer(GravityCompat.START);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

//        loadMoodPlot();

        Log.d(TAG, "onNavigationItemSelected: ****");

        boolean result = false;

        if (id == R.id.nav_mood_selector) {
            result = navigateToFragment(FRAGMENT_TAG_MOOD_CLUSTERS);
        } else if (id == R.id.nav_new_moodgraph) {
            newMoodPlot();
        } else if (id == R.id.nav_open_mood_plot) {
            try {
                openMoodPlot();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (MoodPlotException e) {
                e.printStackTrace();
            }
            closeDrawer();
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        } else if (id == R.id.nav_chart_simple) {
            result = navigateToFragment(FRAGMENT_TAG_CHARTS_SIMPLE);
        } else if (id == R.id.nav_chart_triggers) {
            result = navigateToFragment(FRAGMENT_TAG_TRIGGER_LIST);
        } else if (id == R.id.nav_settings) {
            result = navigateToFragment(FRAGMENT_TAG_PREFERENCES);
        }

        closeDrawer();

        return result;
    }

    /**
     * Show a dialog to select the mood plot.
     *
     * For the uninitiated, the file path is then passed off to onActivityResult.
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    private void openMoodPlot() throws JSONException, IOException, MoodPlotException {
        // Save the current mood plot.
        getOpenHeartService().flush();

        Intent fileIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
//        fileIntent.addCategory(Intent.CATEGORY_OPENABLE);
//        Intent fileIntent = new Intent(Intent.ACTION_GET_CONTENT);
//        Intent chooser = Intent.createChooser(this, "Choose a file");
        fileIntent.setType("application/json:*/*");
        startActivityForResult(fileIntent, REQUEST_CODE_MOOD_PLOT_SELECTED);
    }

    private void newMoodPlot() {
        Intent fileIntent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        fileIntent.setType("application/json:*/*");
        startActivityForResult(fileIntent, REQUEST_CODE_MOOD_PLOT_CREATED);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private boolean doCreateNewMoodPlot(String path) {
        MoodPlot moodPlot = null;
        moodPlot = new MoodPlot(openHeartService.getLoadEnvironment().getEmotions());
        moodPlot.setPath(path);
        try {
            moodPlot.flush();
        } catch (MoodPlotException | JSONException | IOException e) {
            e.printStackTrace();
            return false;
        }
        getOpenHeartService().getServiceEnvironment().insertIntoFileHistory(path);
        getOpenHeartService().setMoodPlot(moodPlot);
        return true;
    }

    @TargetApi(Build.VERSION_CODES.N)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private boolean doOpenMoodPlot(String path) {
        MoodPlot moodPlot = null;
        try {
            moodPlot = MoodPlot.fromJsonFile(path,
                    getOpenHeartService().getLoadEnvironment(), getOpenHeartService().getServiceEnvironment());
        } catch (IOException | JSONException | JsonColorValueException | ParseException | JsonColorMissingValueException | MoodPlotException e) {
            e.printStackTrace();
            Log.w(TAG, "onActivityResult: Could not load mood plot " + path, e);
            moodPlot = new MoodPlot(openHeartService.getLoadEnvironment().getEmotions());
            moodPlot.setPath(path);
            try {
                moodPlot.flush();
            } catch (MoodPlotException | JSONException | IOException e1) {
                Log.e(TAG, "doOpenMoodPlot: ...and creating a new one didn't work." +
                                " I give up.", e1);
                e1.printStackTrace();
                return false;
            }
        }
        getOpenHeartService().getServiceEnvironment().insertIntoFileHistory(path);
        getOpenHeartService().setMoodPlot(moodPlot);
        navigateToFragment(FRAGMENT_TAG_MOOD_CLUSTERS);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Uri uri = null;
        MoodPlot moodPlot = null;
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator;
        String message = null;

        // Open Mood plot.
        if (requestCode == REQUEST_CODE_MOOD_PLOT_SELECTED && data != null) {
            if (data != null) {
                uri = data.getData();
                Log.i(TAG, "Uri: " + uri.getPath());
                path = path + uri.getPath().split(":")[1];
                if (doOpenMoodPlot(path)) {
                    message = new StringBuilder("Opened ").append(path).toString();
                } else {
                    message = new StringBuilder("Could not open ").append(path).toString();
                }
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                navigateToFragment(FRAGMENT_TAG_MOOD_CLUSTERS);
            }
        // Create a new MoodPlot
        } else if (requestCode == REQUEST_CODE_MOOD_PLOT_CREATED && data != null) {
            uri = data.getData();
            Log.i(TAG, "Uri: " + uri.getPath());
            path = path + uri.getPath().split(":")[1];
            if (doOpenMoodPlot(path)) {
                message = new StringBuilder("Created ").append(path).toString();
            } else {
                message = new StringBuilder("Could not create ").append(path).toString();
            }
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
        }

        if (moodPlot != null) {
            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                if (INotifyMoodplotChanged.class.isInstance(fragment)) {
                    Log.d(TAG, "navigateToFragment: Notify moodplot change: " + moodPlot);
                    ((INotifyMoodplotChanged) fragment).onMoodplotChanged(moodPlot);
                }
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public MoodPlot getMoodplot() {
        return isBound && (openHeartService != null) ? openHeartService.getMoodPlot() : null;
    }

    public boolean isBound() {
        return isBound;
    }

    public void setBound(boolean bound) {
        isBound = bound;
    }

    public OpenHeartMoodRecorder getOpenHeartService() {
        if (openHeartService == null) {
            startService();
        }
        return openHeartService;
    }

    public void setOpenHeartService(OpenHeartMoodRecorder openHeartService) {
        this.openHeartService = openHeartService;
    }

    public FileHistory getFileHistoryModel() {
        return fileHistoryModel;
    }

    public void setFileHistoryModel(FileHistory fileHistoryModel) {
        this.fileHistoryModel = fileHistoryModel;
    }
}
