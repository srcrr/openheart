package pro.thesrcrr.openheart.exceptions;

public class TriggerMetricException extends EmotiveException {
    public TriggerMetricException(String message) {
        super(message);
    }
}
