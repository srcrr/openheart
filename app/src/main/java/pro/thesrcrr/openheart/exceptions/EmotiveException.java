package pro.thesrcrr.openheart.exceptions;

public abstract class EmotiveException extends Exception {
    public EmotiveException(String message) {
        super(message);
    }
}
