package pro.thesrcrr.openheart.exceptions;

public class OpenHeartActivityException extends EmotiveException {
    public OpenHeartActivityException(String message) {
        super(message);
    }
}
