package pro.thesrcrr.openheart.fragments;

import android.os.Build;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import pro.thesrcrr.openheart.R;
import pro.thesrcrr.openheart.adapter.TriggerListContent;
import pro.thesrcrr.openheart.adapter.TriggerListItem;
import pro.thesrcrr.openheart.adapter.TriggerListStep;
import pro.thesrcrr.openheart.calendar.UserCalendarEvent;
import pro.thesrcrr.openheart.emote.Emotion;
import pro.thesrcrr.openheart.fragments.TriggerListFragment.OnListFragmentInteractionListener;
import pro.thesrcrr.openheart.fragments.dummy.DummyContent.DummyItem;
import pro.thesrcrr.openheart.trigger.CalendarEventMetric;
import pro.thesrcrr.openheart.trigger.TriggerMetric;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class TriggerItemRecyclerViewAdapter extends RecyclerView.Adapter<TriggerItemRecyclerViewAdapter.ViewHolder> {

    private final TriggerListContent mValues;
    private final OnListFragmentInteractionListener mListener;

    public TriggerItemRecyclerViewAdapter(TriggerListContent items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.trigger_fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.getTriggerListItems().get(position);

        // Distinguish between "Steps" and "Titles"
        Emotion emotion = holder.mItem.getEmotion();
        TriggerMetric triggerMetric = null;
        if (holder.mItem instanceof TriggerListStep) {
            triggerMetric = ((TriggerListStep) holder.mItem).getTriggerMetric();
        }
        final boolean isStep = (emotion != null) && (triggerMetric != null);

        int layoutId = R.layout.trigger_metric_item_title;

        if (isStep) {
            layoutId = R.layout.trigger_metric_item_step;
        }

        View v = LayoutInflater.from(((ViewGroup) holder.mView.getParent()).getContext())
                .inflate(layoutId, (ViewGroup) holder.mView.getParent());

        if (isStep) {
            TextView description = v.findViewById(R.id.textview_trigger_metric_item_step_description),
                    date = v.findViewById(R.id.textview_trigger_metric_item_step_date);
            if (triggerMetric instanceof CalendarEventMetric) {
                UserCalendarEvent event = ((CalendarEventMetric) triggerMetric).getUserCalendarEvent();
                String timeSpan = String.valueOf(DateUtils.getRelativeTimeSpanString(event.getStartTime().getTime()));
                // Set the values
                description.setText(event.getTitle());
                date.setText(timeSpan);
            }
        } else {
            TextView title = v.findViewById(R.id.textview_trigger_metric_item_title);
            title.setText(emotion.getNameAsTitle());
        }

        holder.mView = v;

        holder.mView.setOnClickListener(v1 -> {
            if (null != mListener) {
                // Notify the active callbacks interface (the activity, if the
                // fragment is attached to one) that an item has been selected.
                mListener.onListFragmentInteraction(holder.mItem);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public int getItemCount() {
        return mValues.getTriggerListItems().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public View mView;
        public TriggerListItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
        }

        @Override
        public String toString() {
            return super.toString() ;
        }
    }
}
