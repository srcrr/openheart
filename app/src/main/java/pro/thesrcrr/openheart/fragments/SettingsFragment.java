package pro.thesrcrr.openheart.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import androidx.preference.MultiSelectListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import java.util.Set;

import pro.thesrcrr.openheart.R;
import pro.thesrcrr.openheart.calendar.UserCalendar;
import pro.thesrcrr.openheart.util.PrefGet;

public class SettingsFragment extends PreferenceFragmentCompat {

    public static final String PREFERENCE_KEY_TRIGGER_TIMEFRAME_BEFORE = "preference_key_trigger_timeframe_before";

    public static final float PREFERENCE_DEFAULT_TRIGGER_TIMEFRAME_BEFORE = (float) 0.0;

    public static final int METRIC_TIME_OF_DAY = 1,
                            METRIC_LOCATION = 2,
                            METRIC_CALENDAR = 3,
                            METRIC_PHONE_CALLS = 4;
    private static final String TAG = "OpenHeartSettings";

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preferences_main, rootKey);
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(
                new SharedPreferences.OnSharedPreferenceChangeListener() {
                    @Override
                    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                        if (key == getResources().getString(R.string.preference_key_trigger_metrics)) {
                            Log.d(TAG, "yay!!!");
                        }
                    }
                }
        );
    }

    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        Log.d(TAG, "onPreferenceTreeClick: " + preference.getKey());
        Log.d(TAG, "onPreferenceTreeClick: " + PrefGet.is(preference, R.string.preference_key_trigger_metrics));
        if (PrefGet.is(preference, R.string.preference_key_trigger_metrics)) {
            MultiSelectListPreference p = (MultiSelectListPreference) preference;
            Set<String> values = p.getValues();
            if (values.contains(String.valueOf(METRIC_CALENDAR))) {
                UserCalendar.requestReadPermissions(this.getActivity());
            }
        }
        return super.onPreferenceTreeClick(preference);
    }
}
