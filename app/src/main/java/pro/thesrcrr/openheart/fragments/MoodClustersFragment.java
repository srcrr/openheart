package pro.thesrcrr.openheart.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import org.json.JSONException;

import java.io.IOException;

import pro.thesrcrr.openheart.MainActivity;
import pro.thesrcrr.openheart.R;
import pro.thesrcrr.openheart.emote.Emotion;
import pro.thesrcrr.openheart.emote.EmotionList;
import pro.thesrcrr.openheart.emote.EmotionLoader;
import pro.thesrcrr.openheart.emote.INotifyMoodplotChanged;
import pro.thesrcrr.openheart.emote.MoodPlot;
import pro.thesrcrr.openheart.emote.OpenHeartMoodRecorder;
import pro.thesrcrr.openheart.emote.PrimaryEmotion;
import pro.thesrcrr.openheart.emote.SecondaryEmotion;
import pro.thesrcrr.openheart.emote.TertiaryEmotion;
import pro.thesrcrr.openheart.ui.MoodCluster;

public class MoodClustersFragment extends Fragment implements INotifyMoodplotChanged {
    private static final String TAG = "MoodClustersFragment";
    private static final int LAYOUT_ID = R.layout.fragment_mood_clusters;
    private final MainActivity mainActivity;

    public MoodClustersFragment(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public void onMoodplotChanged(MoodPlot moodPlot) {
        // not implemented
    }

    public interface OnMoodEventListener {
        void onMoodRecorded();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(LAYOUT_ID, container, false);
        try {
            v = addAllMoodTags(v);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onCreateContextMenu(@NonNull ContextMenu menu, @NonNull View v, @Nullable ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    private View addFavoriteMoodTags(View v) {
        Log.d(TAG, "addFavoriteMoodTags: v = " + v);
        return v;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private View addMoodTags(View v) throws IOException {
        v = addFavoriteMoodTags(v);
        Log.d(TAG, "addMoodTags: v = " + v);
        v = addAllMoodTags(v);
        Log.d(TAG, "addMoodTags: v = " + v);
        assert (v != null);
        return v;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private View addPopularMoodTags(View v) {
        MoodPlot moodPlot = this.mainActivity.getMoodplot();
        EmotionList top5 = new EmotionList(moodPlot.getCounts().top(5));
        MoodCluster cluster = v.findViewById(R.id.cluster_favorite_moods);
        cluster.setMoods(top5);
        cluster.createMoodButtons();
        return v;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private View addAllMoodTags(View v) throws IOException {
        EmotionLoader loader = new EmotionLoader(getContext());
        EmotionList emotions = loader.fromResource(R.raw.default_emotions);
        Log.i(TAG, "addAllMoodTags: Loaded " + emotions.size() + " emotions.");
        Log.d(TAG, "addAllMoodTags: type(v) = " + (v));
        MoodCluster cluster = v.findViewById(R.id.cluster_all_moods);
        EmotionList rootEmotions = emotions.getPrimary();
        cluster.setOnEmotionSelectedListener(new MoodCluster.OnEmotionSelectedListener() {
            @Override
            public void onPrimaryEmotionSelected(MoodCluster moodCluster, PrimaryEmotion emotion) {
                moodCluster.removeAllViews();
                moodCluster.setMoods(emotion.getSecondaryEmotions());
                moodCluster.createMoodButtons();
            }

            @Override
            public void onSecondaryEmotionSelected(MoodCluster moodCluster, SecondaryEmotion emotion) {
                moodCluster.removeAllViews();
                moodCluster.setMoods(emotion.getTertiaryEmotions());
                moodCluster.createMoodButtons();
            }

            @Override
            public void onTertiaryEmotionSelected(MoodCluster moodCluster, TertiaryEmotion emotion) {
                recordMood(emotion);
                moodCluster.removeAllViews();
                moodCluster.setMoods(rootEmotions);
                moodCluster.createMoodButtons();
            }
        });
        cluster.setMoods(rootEmotions);
        cluster.createMoodButtons();
        Log.d(TAG, "addAllMoodTags: " + ((MoodCluster) v.findViewById(R.id.cluster_all_moods)).getChildCount());
        return v;
    }

    public void recordMood(Emotion emotion) {
        Intent intent = new Intent(this.getContext(), OpenHeartMoodRecorder.class);
        try {
            intent.setAction(OpenHeartMoodRecorder.ACTION_RECORD_MOOD);
            intent.putExtra(OpenHeartMoodRecorder.EXTRA_MOODPOINT,
                    emotion.toJson().toString());
            getActivity().startService(intent);
            StringBuilder msg = (new StringBuilder()).append("Record Mood: ")
                    .append(emotion.getNameAsTitle());
            Toast.makeText(getContext(), msg.toString(), Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && mainActivity.isBound()) {
//            mainActivity.getOpenHeartService().loadMoodPlot();
        } else {
            Log.w(TAG, "recordMood: Invalid SDK version");
        }
//        mainActivity.notifyFragmentsOfMoodplotChange();
    }
}
