package pro.thesrcrr.openheart.fragments;

import pro.thesrcrr.openheart.exceptions.OpenHeartActivityException;

public class OpenHeartFragmentNotFoundException extends OpenHeartActivityException {
    private String tag;

    public OpenHeartFragmentNotFoundException(String tag) {
        super(
                new StringBuilder("Fragment \"").append(tag)
                                                .append("\" not found").toString()
        );
    }
}
