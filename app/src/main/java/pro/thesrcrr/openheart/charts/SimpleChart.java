package pro.thesrcrr.openheart.charts;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Date;

import pro.thesrcrr.openheart.MainActivity;
import pro.thesrcrr.openheart.R;
import pro.thesrcrr.openheart.emote.Emotion;
import pro.thesrcrr.openheart.emote.INotifyMoodplotChanged;
import pro.thesrcrr.openheart.emote.MoodPlot;

public class SimpleChart extends Fragment implements IChartable, INotifyMoodplotChanged {
    private static final int LAYOUT_ID = R.layout.simple_chart;
    private static final String TAG = "SimpleChart";
    private MainActivity mainActivity;
    private RecyclerView recyclerView = null;
    private OpenHeartSimpleRecyclerAdapter adapter;

    public SimpleChart(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onMoodplotChanged(MoodPlot moodPlot) {
        populateRecyclerView();
    }

    private class OpenHeartSimpleRecyclerAdapter extends RecyclerView.Adapter<OpenHeartSimpleRecyclerAdapter.LinearLayoutViewHolder> {

        private static final int TEXTVIEW_LAYOUT_ID = R.layout.recycler_item_mood_point;

//        private MoodPlot moodPlot;

        public class LinearLayoutViewHolder extends RecyclerView.ViewHolder {
            public LinearLayoutCompat linearLayout;

            // each data item is just a string in this case
            public LinearLayoutViewHolder(LinearLayoutCompat v) {
                super(v);
                this.linearLayout = v;
            }
        }

        public OpenHeartSimpleRecyclerAdapter() {
        }
        
        @NonNull
        @Override
        public OpenHeartSimpleRecyclerAdapter.LinearLayoutViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LinearLayoutCompat recyclerItemMoodPoint = (LinearLayoutCompat) LayoutInflater
                    .from(parent.getContext()).inflate(TEXTVIEW_LAYOUT_ID, parent,
                                          false);
            LinearLayoutViewHolder vh = new LinearLayoutViewHolder(recyclerItemMoodPoint);
            return vh;
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onBindViewHolder(@NonNull LinearLayoutViewHolder holder, int position) {
            TextView mpMood = (TextView) holder.linearLayout.findViewById(R.id.simple_chart_moodpoint_mood);
            TextView mpTime = (TextView) holder.linearLayout.findViewById(R.id.simple_chart_moodpoint_timestamp);
            MoodPlot plot = mainActivity.getOpenHeartService().getMoodPlot();
            Date time = plot.getTimeAt(position);
            Emotion mood = plot.getMoodAt(position);
            Log.d(TAG, "onBindViewHolder: position " + position);
            try {
                mpMood.setText(mood.getNameAsTitle());
                mpTime.setText(time.toString());
                Log.d(TAG, "onBindViewHolder: emotion @ " + position + ": " + mood.getName());
            } catch (NullPointerException e) {
                Log.e(TAG, "onBindViewHolder: position " + position + " :: ", e);
            }
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public int getItemCount() {
            int c = mainActivity.getOpenHeartService().getMoodPlotCount();
            Log.d(TAG, "getItemCount: " + c);
            return c;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new OpenHeartSimpleRecyclerAdapter();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = getActivity().getLayoutInflater().inflate(LAYOUT_ID, container, false);

        LinearLayoutManager llm = new LinearLayoutManager(getContext());

        recyclerView = v.findViewById(R.id.recycler_mood_graph_simple);
        recyclerView.setLayoutManager(llm);

        if (mainActivity.isBound()) {
            mainActivity.getOpenHeartService().loadMoodPlot();
            recyclerView.setAdapter(adapter);
            Log.d(TAG, "onCreateView: moodplot = " + mainActivity.getOpenHeartService().getMoodPlot());
        }

        return v;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void populateRecyclerView() {
//        Log.d(TAG, "populateRecyclerView: " + mainActivity.getOpenHeartService().getMoodPlot());
        adapter = new OpenHeartSimpleRecyclerAdapter();
        recyclerView.swapAdapter(adapter, true);
    }
}
