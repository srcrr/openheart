package pro.thesrcrr.openheart.emote;

import org.json.JSONException;
import org.json.JSONObject;

import pro.thesrcrr.openheart.util.Jsonable;

public final class PrimaryEmotion extends Emotion implements Jsonable {

    EmotionList secondaryEmotions;

    public PrimaryEmotion(int id, String name) {
        super(id, name);
        this.secondaryEmotions = new EmotionList();
    }

    public PrimaryEmotion(int id, String name, JsonColor color) {
        super(id, name, color);
        this.secondaryEmotions = new EmotionList();
    }

    public static PrimaryEmotion fromJson(JSONObject emotionObject, OHLoadEnvironment env) throws JSONException, JsonColorValueException, JsonColorMissingValueException {
        return new PrimaryEmotion(
                emotionObject.getInt(ID),
                emotionObject.getString(NAME),
                JsonColor.fromJson(emotionObject.getJSONObject(COLOR), env)
        );
    }

    public EmotionList getSecondaryEmotions() {
        return secondaryEmotions;
    }

    public void setSecondaryEmotions(EmotionList secondaryEmotions) {
        this.secondaryEmotions = secondaryEmotions;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject obj = super.toJson();
        return obj;
    }
}
