package pro.thesrcrr.openheart.emote;

import org.json.JSONException;
import org.json.JSONObject;

public final class TertiaryEmotion extends Emotion {

    private SecondaryEmotion secondaryEmotion;

    private static final String SECONDARY_EMOTION = "secondaryEmotion";

    public TertiaryEmotion(int id, String name, SecondaryEmotion secondaryEmotion) {
        super(id, name, null);
        this.secondaryEmotion = secondaryEmotion;
        this.secondaryEmotion.getTertiaryEmotions().add(this);
    }

    public TertiaryEmotion(int id, String name, SecondaryEmotion secondaryEmotion, JsonColor color) {
        super(id, name, color);
        this.secondaryEmotion = secondaryEmotion;
        this.secondaryEmotion.getTertiaryEmotions().add(this);
    }

    public static TertiaryEmotion fromJson(JSONObject obj, OHLoadEnvironment env) throws JSONException {
        return new TertiaryEmotion(
                obj.getInt(ID),
                obj.getString(NAME),
                (SecondaryEmotion) env.findEmotionById(obj.getInt(SECONDARY_EMOTION),
                                                       SecondaryEmotion.class)
        );
    }

    public SecondaryEmotion getSecondaryEmotion() {
        return secondaryEmotion;
    }

    public void setSecondaryEmotion(SecondaryEmotion secondaryEmotion) {
        this.secondaryEmotion = secondaryEmotion;
    }

    public PrimaryEmotion getPrimaryEmotion() {
        return this.getSecondaryEmotion().getPrimaryEmotion();
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject obj = super.toJson();
        try {
            obj = obj.put(SECONDARY_EMOTION, getSecondaryEmotion().getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }
}
