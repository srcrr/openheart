package pro.thesrcrr.openheart.emote.exc;

public class MoodPlotException extends Exception {

    public MoodPlotException(String reason) {
        super(reason);
    }
}
