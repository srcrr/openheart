package pro.thesrcrr.openheart.emote;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import androidx.annotation.RequiresApi;

import org.apache.commons.collections4.list.TreeList;

import java.io.File;

import pro.thesrcrr.openheart.calendar.UserCalendar;
import pro.thesrcrr.openheart.emote.data.FileHistory;

public class EmotiveServiceEnvironment {

    private static final String TAG = "EServiceEnv";
    private final FileHistory fileHistory;
    private TreeList<UserCalendar> userCalendars;

    public final static String DEFAULT_FILE_NAME = "OpenHeartChart.json";
    public final static String DEFAULT_PATH = Environment.getExternalStorageDirectory() + File.separator + DEFAULT_FILE_NAME;

    public EmotiveServiceEnvironment(Context context) {
        this.fileHistory = FileHistory.getInstance(context);
//        this.userCalendars = UserCalendar.getAll(context);
    }

    public FileHistory getFileHistory() {
        Log.d(TAG, "getFileHistory: File History");
        return fileHistory;
    }

    public String getLastOpenedPath() {
        return (String) this.fileHistory.get(0);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void insertIntoFileHistory(String path) {
        Log.d(TAG, "insertIntoFileHistory: " + path);
        this.fileHistory.insert(path);
    }

    public TreeList<UserCalendar> getUserCalendars() {
        return userCalendars;
    }

    public void setUserCalendars(TreeList<UserCalendar> userCalendars) {
        this.userCalendars = userCalendars;
    }
}
