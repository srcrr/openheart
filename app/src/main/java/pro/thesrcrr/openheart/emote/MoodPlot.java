package pro.thesrcrr.openheart.emote;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import org.apache.commons.collections4.list.TreeList;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;

import pro.thesrcrr.openheart.R;
import pro.thesrcrr.openheart.calendar.UserCalendar;
import pro.thesrcrr.openheart.calendar.UserCalendarEvent;
import pro.thesrcrr.openheart.emote.exc.MoodPlotException;
import pro.thesrcrr.openheart.exceptions.TriggerMetricException;
import pro.thesrcrr.openheart.fragments.SettingsFragment;
import pro.thesrcrr.openheart.trigger.CalendarEventMetric;
import pro.thesrcrr.openheart.trigger.TriggerMetric;
import pro.thesrcrr.openheart.trigger.TriggerMetricFactory;
import pro.thesrcrr.openheart.util.HitList;
import pro.thesrcrr.openheart.util.Jsonable;
import pro.thesrcrr.openheart.util.PrefGet;

public class MoodPlot extends TreeMap<Date, Emotion> implements Jsonable {

    public static final String FILE_PATTERN = ".*\\.json$";
    private static final String TAG = "MoodPlot";
    private static final int DEFAULT_INDENT_SPACES = 2;
    private String path = null;


    // additional to be exported to/from JSON
    private EmotionList customEmotions;
    private HitList<Emotion> counts;
    private HashMap<Date, TriggerMetric> triggerMetrics;

    public static final String CUSTOM_EMOTIONS = "customEmotions",
                               META = "meta",
                               TRIGGER_METRICS = "trigger_metrics",
                               POINTS = "points",
                               COUNTS = "counts"
                                        ;
    private final static String DATE_FORMAT_STR = "yyyy/MM/dd HH:mm:ss.SSS";
    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT_STR);

    private MoodPlotChangedListener moodPlotChangedListener;
    private int indentSpaces = DEFAULT_INDENT_SPACES;

    public MoodPlot(EmotionList customEmotions) {
        super((o1, o2) -> o1.compareTo(o2));
        this.customEmotions = customEmotions;
        this.counts = new HitList<>();
        triggerMetrics = new HashMap<>();
    }

    public EmotionList getCustomEmotions() {
        return customEmotions;
    }

    public void setCustomEmotions(EmotionList customEmotions) {
        this.customEmotions = customEmotions;
    }

    /**
     * Get the key (date) at the position in the list.
     * @param pos Index of the list.
     * @return Date at index `pos`.
     */
    public Date getTimeAt(int pos) {
        Set<Date> l = keySet();
        return (Date) l.toArray()[pos];
    }

    public Emotion getMoodAt(int pos) {
        return get(getTimeAt(pos));
    }

    public static String dateToKey(Date k) {
//        Log.d(TAG, "dateToKey: '" + k + "' => '" + DATE_FORMAT.format(k) + "'");
        return DATE_FORMAT.format(k);
    }

    public static Date keyToDate(String k) throws ParseException {
//        Log.d(TAG, "dateToKey: '" + k + "' => '" + DATE_FORMAT.parse(k) + "'");
        return DATE_FORMAT.parse(k);
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject obj = new JSONObject();

        // Hit List counts
        JSONObject jCounts = new JSONObject();
        for (Emotion k : counts) {
            jCounts = jCounts.put(String.valueOf(k.getId()), counts.get(k));
        }

        // Trigger Metrics
        JSONObject jTriggerMetrics = new JSONObject();
        for (Date k : triggerMetrics.keySet())
            jTriggerMetrics = jTriggerMetrics.put(dateToKey(k), triggerMetrics.get(k).toJson());

        // Points
        JSONObject jPoints = new JSONObject();
        for (Date k : this.keySet())
            jPoints = jPoints.put(dateToKey(k), this.get(k).toJson());

        obj = obj.put(TRIGGER_METRICS, jTriggerMetrics)
                 .put(COUNTS, jCounts)
                 .put(POINTS, jPoints)
                 ;


        return obj;
    }

    public static MoodPlot fromJson(JSONObject obj, OHLoadEnvironment env, EmotiveServiceEnvironment ese) throws JSONException, JsonColorValueException, JsonColorMissingValueException, ParseException {
        MoodPlot plot = new MoodPlot(new EmotionList());
        Date date = null;
        HitList<Emotion> counts = new HitList<>();
        HashMap<Date, TriggerMetric> triggerMetrics = new HashMap<>();

        if (!obj.keys().hasNext()) {
            return plot;
        }
        try {

            JSONObject jPoints = obj.getJSONObject(POINTS),
                       jTriggerMetrics = obj.getJSONObject(TRIGGER_METRICS),
                       jCounts = obj.getJSONObject(COUNTS);
            // Points
            for (Iterator<String> it = jPoints.keys(); it.hasNext(); ) {
                String k = it.next();
                plot.put(keyToDate(k), EmotionFactory.fromJson(new JSONObject(jPoints.getString(k)),
                         env));
            }

            // Trigger Metrics
            for (Iterator<String> it = jTriggerMetrics.keys(); it.hasNext(); ) {
                String k = it.next();
                date = keyToDate(k);
                TriggerMetric metric = TriggerMetricFactory.fromJson(
                        new JSONObject(jTriggerMetrics.getString(k)), ese
                );
                triggerMetrics.put(date, metric);
            }
            plot.setTriggerMetrics(triggerMetrics);

            // Counts
            for (Iterator<String> it = jCounts.keys(); it.hasNext(); ) {
                String k = it.next();
                int id = Integer.valueOf(k).intValue();
                Emotion emotion = env.findEmotionById(id, Emotion.class);
                counts.add(emotion, jCounts.getInt(k));
            }
            plot.setCounts(counts);

        } catch (NullPointerException | TriggerMetricException e) {
            Log.e(TAG, "fromJson: ", e);
            plot = new MoodPlot(env.getEmotions());
            plot.setPath(EmotiveServiceEnvironment.DEFAULT_FILE_NAME);
            return plot;
        }
        return plot;
    }

    @Nullable
    @Override
    public Emotion put(Date key, Emotion value) {
        Emotion e = super.put(key, value);
//        this.moodPlotChangedListener.onPut(key, value);
        return e;
    }

    @Nullable
    @Override
    public Emotion remove(@Nullable Object key) {
        Emotion e = super.remove(key);
//        moodPlotChangedListener.onRemoved(e);
        return e;
    }

    /**
     * Open or Create a MoodPlot from a JSON File
     * @param path Path to the MoodPlot
     * @param autoCreate `true` if the file should be created if it doesn't exist.
     * @param env
     * @return a new MoodPlot, even if it's empty
     * @throws IOException
     * @throws JSONException
     * @throws JsonColorValueException
     * @throws ParseException
     * @throws JsonColorMissingValueException
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static MoodPlot fromJsonFile(String path, boolean autoCreate, OHLoadEnvironment env, EmotiveServiceEnvironment ese) throws IOException, JSONException, JsonColorValueException, ParseException, JsonColorMissingValueException, MoodPlotException {
        String rawJson;
        MoodPlot plot;
        try(FileInputStream inputStream = new FileInputStream(path)) {
            rawJson = IOUtils.toString(inputStream);
        }
        File f = new File(path);
        if(!f.exists()) {
            Log.w(TAG, "fromJsonFile: path " + path + " does not exist. Creating");
            plot = new MoodPlot(new EmotionList());
            plot.setPath(path);
            plot.flush();
        } else {
            Log.d(TAG, "fromJsonFile: Raw json: \"" + rawJson + "\"");
            JSONObject obj = new JSONObject(rawJson);
            plot = MoodPlot.fromJson(obj, env, ese);
            plot.setPath(path);
        }
//        Log.d(TAG, "flush: Flushing...");
//        for (String line : plot.toJson().toString(2).split("\n")) {
//            Log.d(TAG, "flush: " + line);
//        }
//        plot.flush();
//        plot.sort();
        Log.d(TAG, "fromJsonFile: moodPlot: " + plot);
        return plot;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static MoodPlot fromJsonFile(String path, OHLoadEnvironment env, EmotiveServiceEnvironment ese) throws IOException, JSONException, JsonColorValueException, ParseException, JsonColorMissingValueException, MoodPlotException {
        return MoodPlot.fromJsonFile(path, false, env, ese);
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void record(Context context, Emotion emotion, Date time) {
        this.put(time, emotion);

        // ---- Add to HitList
        this.counts.add(emotion);
        for (Emotion e : counts) {
            Log.i(TAG, "record: count(" + e.getName() + ") => " + counts.get(e));
        }

        // ---- Also record the trigger metrics

        // Calendar trigger
        if (PrefGet.arrayHasValue(context, R.string.preference_key_trigger_metrics, SettingsFragment.METRIC_CALENDAR)) {
            Log.d(TAG, "record: [trigger] calendar");
            TreeList<UserCalendarEvent> events = new TreeList<>();
            TreeList<UserCalendar> calendars = UserCalendar.getAll(context);
//            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
            Log.d(TAG, "record: timeframe_before=" + PrefGet.getString(context, R.string.preference_key_trigger_timeframe_after, "[NULL]"));
            float hoursBefore = PrefGet.getFloat(context, R.string.preference_key_trigger_timeframe_after, SettingsFragment.PREFERENCE_DEFAULT_TRIGGER_TIMEFRAME_BEFORE);
            Date start = Date.from(time.toInstant().minusSeconds(((long) hoursBefore * 60 * 60)));
            for (UserCalendar calendar : calendars) {
                events.addAll(calendar.eventsBetween(start, time));
            }
            Log.d(TAG, "record: Found " + events.size() + " events between "
                    + start.toString() + " and " + time.toString());
            for (UserCalendarEvent event : events) {
                triggerMetrics.put(time, new CalendarEventMetric(time, event));
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void record(Context context, Emotion emotion) {
        record(context, emotion, Calendar.getInstance().getTime());
    }

    public void flush() throws MoodPlotException, JSONException, IOException {
        if (this.path == null) {
            throw new MoodPlotException("`path` is null");
        }
        try(FileOutputStream outputStream = new FileOutputStream(path)) {
            IOUtils.write(this.toJson().toString(indentSpaces), outputStream);
        }
    }

    public MoodPlotChangedListener getMoodPlotChangedListener() {
        return moodPlotChangedListener;
    }

    public void setMoodPlotChangedListener(MoodPlotChangedListener moodPlotChangedListener) {
        if (this.moodPlotChangedListener != null) {
            Log.w(TAG, "setMoodPlotChangedListener: Changing MoodPlotChangeListener");
        }
        this.moodPlotChangedListener = moodPlotChangedListener;
    }

    public void setIndentSpaces(int indentSpaces) {
        this.indentSpaces = indentSpaces;
    }

    public HitList<Emotion> getCounts() {
        return counts;
    }

    public void setCounts(HitList<Emotion> counts) {
        this.counts = counts;
    }

    public HashMap<Date, TriggerMetric> getTriggerMetrics() {
        return triggerMetrics;
    }

    public void setTriggerMetrics(HashMap<Date, TriggerMetric> triggerMetrics) {
        this.triggerMetrics = triggerMetrics;
    }

    public void addTriggerMetric(TriggerMetric metric) {
        triggerMetrics.put(metric.getDate(), metric);
    }

    public interface MoodPlotChangedListener {
        void onPut(Date key, Emotion mood);
        void onRemoved(Emotion mood);
    }

    public TreeList<TriggerMetric> getMetricsForMood(Emotion mood) {
        // First get a list of times associated with the mood.
        TreeList<Date> times = new TreeList<>();
        TreeList<TriggerMetric> metrics = new TreeList<>();
        for (Date k : keySet())
            if (get(k) == mood) {
                times.add(k);
            }

        // now add the associated times for the mood
        for (Date t : times)
            if (!metrics.contains(triggerMetrics.get(t)))
                metrics.add(triggerMetrics.get(t));

        return metrics;
    }

    /**
     * Get a mapping of all emotions -> triggers.
     * @return
     */
    public HashMap<Emotion, TreeList<TriggerMetric>> getEmotionTriggerMapping() {
        HashMap<Emotion, TreeList<TriggerMetric>> map = new HashMap<>();
        for (Emotion e : values()) {
            if (map.containsKey(e)) {
                map.get(e).addAll(getMetricsForMood(e));
            } else {
                map.put(e, getMetricsForMood(e));
            }
        }
        return map;
    }
}
