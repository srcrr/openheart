package pro.thesrcrr.openheart.emote;

public class JsonColorMissingValueException extends Exception {
    private final String field;

    public JsonColorMissingValueException(String field) {
        this.field = field;
    }

    public String getField() {
        return field;
    }

    @Override
    public String toString() {
        return (new StringBuilder()).append("Missing field `")
                                    .append(field)
                                    .append("`.").toString();
    }
}
