package pro.thesrcrr.openheart.emote;

import org.json.JSONException;
import org.json.JSONObject;

import pro.thesrcrr.openheart.util.Jsonable;

public class Emotion implements Jsonable {

    private static final String TAG = "OpenHeart";
    private int id;
    private String name;
    private JsonColor color;

    protected static String ID = "id";
    protected static String NAME = "name";
    protected static String COLOR = "color";
    protected static final String CLASS = "class";
    private static final int DEFAULT_INDENT_SPACES = 2;

    public JsonColor getColor() {
        return color;
    }

    public void setColor(JsonColor color) {
        this.color = color;
    }

    public Emotion(int id, String name, JsonColor color) {
        this.id = id;
        this.name = name;
        this.color = color;
    }

    public Emotion (int id, String name) {
        this(id, name, null);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNameAsTitle() {
        return (new StringBuilder()).append(String.valueOf(this.name.charAt(0)).toUpperCase())
                                    .append(this.name.substring(1)).toString();
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        try {
            return this.toJson().toString();
        } catch (JSONException e) {
            e.printStackTrace();
            StringBuilder b = new StringBuilder();
            b.append("<Emotion (id=")
                    .append(id)
                    .append(", name=\"")
                    .append(name)
                    .append("\"")
                    .append(")>");
            final String s = b.toString();
            return s;
        }
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject obj = new JSONObject();
        try {
            obj = obj.put(CLASS, getClass().getName());
            obj = obj.put(ID, Integer.valueOf(getId()));
            obj = obj.put(NAME, getName());
//            obj.put(COLOR, getColor().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        Log.d(TAG, "toJson: " + obj);
        return obj;
    }
}
