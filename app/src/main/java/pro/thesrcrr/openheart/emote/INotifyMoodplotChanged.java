package pro.thesrcrr.openheart.emote;

public interface INotifyMoodplotChanged {
    void onMoodplotChanged(MoodPlot moodPlot);
}
