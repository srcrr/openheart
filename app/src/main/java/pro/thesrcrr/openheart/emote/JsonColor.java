package pro.thesrcrr.openheart.emote;

import org.json.JSONException;
import org.json.JSONObject;

import pro.thesrcrr.openheart.util.Jsonable;

public class JsonColor implements Jsonable {
    private int red,
                blue,
                green,
                alpha;
    public static final String RED = "red",
                               GREEN = "green",
                               BLUE = "blue",
                               ALPHA = "alpha";
    public final static int VALUE_MIN = 0,
                            VALUE_MAX = 255;

    public JsonColor(int red, int green, int blue) throws JsonColorValueException {
        // TODO: color error checking
        this.red = checkColor(JsonColor.RED, red);
        this.green = checkColor(JsonColor.GREEN, green);
        this.blue = checkColor(JsonColor.BLUE, blue);
        this.alpha = 1;
    }

    public JsonColor(int red, int green, int blue, int alpha) throws JsonColorValueException {
        this(red, green, blue);
        // TODO color error checking
        this.alpha = checkColor(JsonColor.ALPHA, alpha);
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject obj = new JSONObject();
        return obj.put(RED, this.red)
                  .put(GREEN, this.green)
                  .put(BLUE, this.blue)
                  .put(ALPHA, this.alpha);
    }

    public static JsonColor fromJson(JSONObject arr, OHLoadEnvironment env) throws JSONException, JsonColorValueException, JsonColorMissingValueException {
        int alpha = arr.has(ALPHA) ? arr.getInt(ALPHA) : 1;
        if (!arr.has(RED)) {
            throw new JsonColorMissingValueException(RED);
        }
        if (!arr.has(GREEN)) {
            throw new JsonColorMissingValueException(GREEN);
        }
        if (!arr.has(BLUE)) {
            throw new JsonColorMissingValueException(BLUE);
        }
        return new JsonColor(
                arr.getInt(RED),
                arr.getInt(GREEN),
                arr.getInt(BLUE),
                alpha
        );
    }

    private int checkColor(String field, int value) throws JsonColorValueException {
        if (value < VALUE_MIN || value > VALUE_MAX) {
            throw new JsonColorValueException(field, value, VALUE_MIN, VALUE_MAX);
        }
        return value;
    }

    public int getRed() {
        return red;
    }

    public void setRed(int red) throws JsonColorValueException {
        this.red = checkColor(JsonColor.RED, red);
    }

    public int getGreen() {
        return green;
    }

    public void setGreen(int green) throws JsonColorValueException {
        this.green = checkColor(JsonColor.GREEN, green);
    }

    public int getBlue() {
        return blue;
    }

    public void setBlue(int blue) throws JsonColorValueException {
        this.blue = checkColor(JsonColor.BLUE, blue);
    }

    public int getAlpha() {
        return alpha;
    }

    public void setAlpha(int alpha) throws JsonColorValueException {
        this.alpha = checkColor(JsonColor.ALPHA, alpha);
    }
}
