package pro.thesrcrr.openheart.emote;

public class JsonColorValueException extends Exception {
    private final String field;
    private final int value, min, max;

    public JsonColorValueException(String field, int value, int min, int max) {
        this.field = field;
        this.value = value;
        this.min = min;
        this.max = max;
    }

    @Override
    public String toString() {
        return (new StringBuilder()).append("Invalid value (")
                                    .append(this.value)
                                    .append(") for `")
                                    .append(this.field)
                                    .append("`. Must be between [")
                                    .append(this.min)
                                    .append(", ")
                                    .append(this.max)
                                    .append("]")
                                    .toString();
    }

    public String getField() {
        return field;
    }

    public int getValue() {
        return value;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }
}
