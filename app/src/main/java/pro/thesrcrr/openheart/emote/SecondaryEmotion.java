package pro.thesrcrr.openheart.emote;

import org.json.JSONException;
import org.json.JSONObject;

import pro.thesrcrr.openheart.util.Jsonable;

public final class SecondaryEmotion extends Emotion implements Jsonable {

    private PrimaryEmotion primaryEmotion;
    private EmotionList tertiaryEmotions;

    private static String PRIMARY_EMOTION = "primaryEmotion";

    public SecondaryEmotion(int id, String name, PrimaryEmotion primaryEmotion) {
        super(id, name);
        this.primaryEmotion = primaryEmotion;
        this.primaryEmotion.secondaryEmotions.add(this);
        this.tertiaryEmotions = new EmotionList();
    }

    public SecondaryEmotion(int id, String name, PrimaryEmotion primaryEmotion, JsonColor color) {
        super(id, name, color);
        this.primaryEmotion = primaryEmotion;
        this.primaryEmotion.secondaryEmotions.add(this);
        this.tertiaryEmotions = new EmotionList();
    }

    public PrimaryEmotion getPrimaryEmotion() {
        return primaryEmotion;
    }

    public void setPrimaryEmotion(PrimaryEmotion primaryEmotion) {
        this.primaryEmotion = primaryEmotion;
    }

    public EmotionList getTertiaryEmotions() {
        if (tertiaryEmotions == null) {
            tertiaryEmotions = new EmotionList();
        }
        return tertiaryEmotions;
    }

    public void setTertiaryEmotions(EmotionList tertiaryEmotions) {
        this.tertiaryEmotions = tertiaryEmotions;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject obj = super.toJson();
        obj = obj.put(PRIMARY_EMOTION, primaryEmotion.getId());
        return obj;
    }

    public static SecondaryEmotion fromJson(JSONObject obj, OHLoadEnvironment env) throws JSONException {
        PrimaryEmotion primaryEmotion = null;
        for (Emotion e : env.getEmotions()) {
            if (e.getId() == obj.getInt(PRIMARY_EMOTION)) {
                primaryEmotion = (PrimaryEmotion) e;
                break;
            }
        }
        return new SecondaryEmotion(
                obj.getInt(ID),
                obj.getString(NAME),
                primaryEmotion
        );
    }
}
