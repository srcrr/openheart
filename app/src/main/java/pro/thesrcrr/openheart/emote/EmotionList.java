package pro.thesrcrr.openheart.emote;

import java.util.LinkedList;
import java.util.List;

public class EmotionList extends LinkedList<Emotion> {

    public EmotionList() {
        super();
    }

    public EmotionList(List<Emotion> list) {
        for (Emotion e : list)
            add(e);
    }

    public EmotionList getPrimary () {
        EmotionList emotions = new EmotionList();
        for (Emotion e : this)
            if (e instanceof PrimaryEmotion)
                emotions.add((PrimaryEmotion) e);
        return emotions;
    }

    public EmotionList getSecondary () {
        EmotionList emotions = new EmotionList();
        for (Emotion e : this)
            if (e instanceof SecondaryEmotion)
                emotions.add((SecondaryEmotion) e);
        return emotions;
    }

    public EmotionList getTertiary () {
        EmotionList emotions = new EmotionList();
        for (Emotion e : this)
            if (e instanceof TertiaryEmotion)
                emotions.add((TertiaryEmotion) e);
        return emotions;
    }
}
