package pro.thesrcrr.openheart.emote;

public class OHLoadEnvironment {
    private static final String TAG = "OHLoadEnvironment";
    private EmotionList emotions;

    public OHLoadEnvironment(EmotionList emotions) {
        this.emotions = emotions;
    }

    public EmotionList getEmotions() {
        return emotions;
    }

    public void setEmotions(EmotionList emotions) {
        this.emotions = emotions;
    }

    public Emotion findEmotionById(int id, Class<? extends Emotion> cls) {
        assert emotions != null;
        for (Emotion e : emotions) {
//            Log.d(TAG, "findEmotionById: " + e.getId() + " == " + id + "?");
            if ((cls.isInstance(e)) && e.getId() == id) {
//                Log.d(TAG, "findEmotionById: TRUE!");
                return e;
            }
        }
//        Log.w(TAG, "findEmotionById: Emotion with ID " + id + " not found." );
        return null;
    }
}
