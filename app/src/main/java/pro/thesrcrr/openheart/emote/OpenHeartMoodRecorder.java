package pro.thesrcrr.openheart.emote;

import android.annotation.TargetApi;
import android.app.IntentService;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import org.apache.commons.collections4.list.TreeList;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Time;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import pro.thesrcrr.openheart.R;
import pro.thesrcrr.openheart.calendar.UserCalendarEvent;
import pro.thesrcrr.openheart.emote.exc.MoodPlotException;
import pro.thesrcrr.openheart.trigger.CalendarEventMetric;
import pro.thesrcrr.openheart.trigger.TimeOfDayMetric;
import pro.thesrcrr.openheart.trigger.TriggerMetric;
import pro.thesrcrr.openheart.util.PrefGet;

import static pro.thesrcrr.openheart.fragments.SettingsFragment.METRIC_CALENDAR;
import static pro.thesrcrr.openheart.fragments.SettingsFragment.METRIC_TIME_OF_DAY;
import static pro.thesrcrr.openheart.fragments.SettingsFragment.PREFERENCE_DEFAULT_TRIGGER_TIMEFRAME_BEFORE;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class OpenHeartMoodRecorder extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    public static final String ACTION_RECORD_MOOD = "pro.thesrcrr.openheart.emote.action.RECORD_MOOD";
    public static final String ACTION_RECORD_MOOD_AT_TIME = "pro.thesrcrr.openheart.emote.action.RECORD_MOOD_AT_TIME";
    public static final String ACTION_OPEN_MOODPLOT = "pro.thesrcrr.openheart.emote.action.OPEN_MOODPLOT";
    public static final String ACTION_LOAD_DEFAULT_MOODS = "pro.thesrcrr.openheart.emote.action.LOAD_DEFAULT_MOODS",
                               ACTION_GET_MOOD_PLOT_SIZE = "pro.thesrcrr.openheart.emote.action.GET_MOOD_PLOT_SIZE",
                               ACTION_GET_MOOD_POINT_AT = "pro.thesrcrr.openheart.emote.action.GET_MOOD_PLOT_AT"
    ;

    // TODO: Rename parameters
    public static final String EXTRA_MOODPOINT = "pro.thesrcrr.openheart.emote.extra.MOODPOINT";
    public static final String EXTRA_MOOD_TIME = "pro.thesrcrr.openheart.emote.extra.MOOD_TIME";
    public static final String EXTRA_MOODPLOT_PATH = "pro.thesrcrr.openheart.emote.extra.MOODPLOT_PATH";
    public static final String EXTRA_ALL_EMOTIONS = "pro.thesrcrr.openheart.emote.extra.ALL_EMOTIONS";
    private static final int BUFFER_SIZE = 1024;
    private static final String TAG = "OpenHeartMoodRecorder";
    private MoodPlot moodPlot;
    private EmotionList defaultEmotions;
    private EmotiveServiceEnvironment serviceEnvironment;
    private OHLoadEnvironment loadEnvironment;
    private IBinder binder = new MoodRecorderBinder();

    public void setServiceEnvironment(EmotiveServiceEnvironment serviceEnvironment) {
        this.serviceEnvironment = serviceEnvironment;
    }


    public class MoodRecorderBinder extends Binder {
        public OpenHeartMoodRecorder getService() {
            return OpenHeartMoodRecorder.this;
        }
    }

    public OpenHeartMoodRecorder() {
        super("OpenHeartMoodRecorder");
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            Emotion mood = null;
            JSONObject obj = null;
            Time date = null;

            Log.d(TAG, "onHandleIntent: Recieved intent");
            Log.d(TAG, "onHandleIntent:   action: " + action);

            if (moodPlot == null)
                loadMoodPlot();

            /**
             * Mood Loading
             **/
            if (ACTION_LOAD_DEFAULT_MOODS.equals(action)) {
            }

            /**
             * Mood recording
             */
            if (ACTION_RECORD_MOOD.equals(action) || ACTION_RECORD_MOOD_AT_TIME.equals(action)) {
                assert defaultEmotions != null;
                String moodPointRaw = intent.getStringExtra(EXTRA_MOODPOINT);
                try {
                    obj = new JSONObject(moodPointRaw);
                    mood = EmotionFactory.fromJson(obj, loadEnvironment);
                } catch (JSONException | JsonColorValueException | JsonColorMissingValueException e) {
                    Log.e(TAG, "Could not parse delivered MoodPoint");
                    Log.e(TAG, "raw:");
                    for (String s : moodPointRaw.split("[\n\r]")) {
                        Log.e(TAG, "| ");
                    }
                    e.printStackTrace();
                }
            }

            if (ACTION_RECORD_MOOD_AT_TIME.equals(action)) {
                date = Time.valueOf(intent.getStringExtra(EXTRA_MOOD_TIME));
            }

            if (ACTION_RECORD_MOOD_AT_TIME.equals(action) || ACTION_RECORD_MOOD.equals(action)) {
                try {
                    if (date != null) {
                        recordMood(mood, date);
                    } else {
                        recordMood(mood);
                    }
                    flush();
                    loadMoodPlot();
                } catch (IOException | JSONException | MoodPlotException e) {
                    Log.e(TAG, "Flushing to `" + moodPlot.getPath() + "`");
                    e.printStackTrace();
                }
            }

            /**
             * Moodplot file handling
             */
            if (ACTION_OPEN_MOODPLOT.equals(action)) {
                final String path = intent.getStringExtra(EXTRA_MOODPLOT_PATH);
                try {
                    moodPlot = MoodPlot.fromJsonFile(path, loadEnvironment, getServiceEnvironment());
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                } catch (JsonColorMissingValueException | ParseException | JsonColorValueException e) {
                    Log.e(TAG, "opening file `" + path + "`");
                    e.printStackTrace();
                } catch (MoodPlotException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Load default emotions for OpenHeart
     */
    private void loadDefaultEmotions() {
        EmotionLoader loader = new EmotionLoader(this);
        try {
            Log.d(TAG, "onHandleIntent: Loading default emotions");
            defaultEmotions = loader.fromResource(R.raw.default_emotions);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setMoodPlot(MoodPlot moodPlot) {
        this.moodPlot = moodPlot;
    }

    /**
     * Write the MoodPlot to the currently-opened JSON file, inserting it into the history.
     * @throws IOException
     * @throws JSONException
     * @throws MoodPlotException
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void flush() throws IOException, JSONException, MoodPlotException {
        if (moodPlot == null) {
            Log.w(TAG, "flush: No Mood Plot loaded", new NullPointerException());
            return;
        }
        serviceEnvironment.insertIntoFileHistory(moodPlot.getPath());
        Log.d(TAG, "flush: Flushing...");
        for (String line : moodPlot.toJson().toString(2).split("\n")) {
            Log.d(TAG, "flush: " + line);
        }
        moodPlot.flush();
    }

    @Override
    public void onStart(@Nullable Intent intent, int startId) {
        super.onStart(intent, startId);
        Log.i(TAG, "onStart: " + this.getClass().getName());
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate() {
        super.onCreate();
        serviceEnvironment = new EmotiveServiceEnvironment(getBaseContext());
        loadDefaultEmotions();
        loadMoodPlot();
        if (moodPlot == null) {
            Log.d(TAG, "onCreate: No mood plot found. Creating...");
            moodPlot = new MoodPlot(defaultEmotions);
            moodPlot.setPath(EmotiveServiceEnvironment.DEFAULT_PATH);
            try {
                moodPlot.flush();
            } catch (MoodPlotException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    /** Client Methods **/

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void loadMoodPlot() {
        loadDefaultEmotions();
        loadEnvironment = new OHLoadEnvironment(defaultEmotions);
        String lastPath = serviceEnvironment.getLastOpenedPath();
        lastPath = (lastPath == null || lastPath.isEmpty()) ? serviceEnvironment.DEFAULT_PATH: lastPath;
        Log.d(TAG, "loadMoodPlot: lastpath=" + lastPath);
        try {
            moodPlot = MoodPlot.fromJsonFile(lastPath, true, loadEnvironment, getServiceEnvironment());
//            MoodPlot mp2 = MoodPlot.fromJsonFile(lastPath, true, loadEnvironment, getServiceEnvironment());
            Log.i(TAG, "loadMoodPlot: Mood Plot loaded: " + moodPlot);
        } catch (IOException | JSONException | JsonColorValueException | ParseException | JsonColorMissingValueException | MoodPlotException e) {
            e.printStackTrace();
//            showErrorNotification("Could not load Mood Plot from " + lastPath, e);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public MoodPlot getMoodPlot() {
        if (moodPlot != null) {
            loadMoodPlot();
        }
        return moodPlot;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public int getMoodPlotCount() {
        if (moodPlot != null) {
            loadMoodPlot();
        }
        return moodPlot.keySet().size();
    }

    public OHLoadEnvironment getLoadEnvironment () {
        return loadEnvironment;
    }

    public EmotiveServiceEnvironment getServiceEnvironment() {
        return serviceEnvironment;
    }


    /**
     * Record a trigger at a specific date.
     * @param time Time at which the trigger occurs.
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void recordTrigger(Date time) {
        float timeBefore = PrefGet.getFloat(this,
                                            R.string.preference_key_trigger_timeframe_after,
                                            PREFERENCE_DEFAULT_TRIGGER_TIMEFRAME_BEFORE);

        // Create a new metrics list
        TreeList<TriggerMetric> metrics = new TreeList<>();

        // Get user preferences on what triggers to include.
        int [] triggers = PrefGet.getIntArray(this, R.string.preference_key_trigger_metrics);

        // Create a trigger point as the current date - hours offset.
        Date triggerPoint = new Date();
        triggerPoint.setTime(time.toInstant().minusSeconds((long) (timeBefore * 60 * 60)).toEpochMilli());

        // Go through each of the selected triggers (from preferences) and create a new trigger metric.
        for (int t : triggers) {
            switch(t) {
                case METRIC_TIME_OF_DAY:
                    metrics.add(new TimeOfDayMetric(time));
                case METRIC_CALENDAR:
                    TreeList<UserCalendarEvent> events = UserCalendarEvent.getAllBetween(this, triggerPoint, time);
                    for (UserCalendarEvent event : events)
                        metrics.add(new CalendarEventMetric(time, event));
            }
        }

        for (TriggerMetric metric : metrics) {
            moodPlot.addTriggerMetric(metric);
        }
    }

    /**
     * Record a mood at a given time.
     * @param mood Mood to record
     * @param time Time at which to record the mood.
     * @throws JSONException
     */
    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void recordMood(Emotion mood, Date time) throws JSONException {
        moodPlot.record(getApplicationContext(), mood, time);
        recordTrigger(time);
        Log.d(TAG, "recordMood: " + mood + "@ " + time.toString());
        try {
            moodPlot.flush();
            loadMoodPlot();
        } catch (MoodPlotException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Record a mood now.
     * @param mood Mood to record.
     * @throws JSONException
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void recordMood(Emotion mood) throws JSONException {
        recordMood(mood, Calendar.getInstance().getTime());
    }
}
