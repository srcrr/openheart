package pro.thesrcrr.openheart.emote;
import com.opencsv.bean.CsvBindByName;

public class EmotionVisitor {
    /**
     * Essentially a proxy binding for emotions.
     * See http://opencsv.sourceforge.net/#annotating_by_header_name for details.
     */
    @CsvBindByName
    private int id;
    @CsvBindByName
    private String primary;
    @CsvBindByName
    private String secondary;
    @CsvBindByName
    private String tertiary;
    @CsvBindByName
    private String color;
    @CsvBindByName
    private String icon;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrimary() {
        if (primary == null)
            return null;
        if (primary.length() == 0)
            return null;
        return primary;
    }

    public void setPrimary(String primary) {
        this.primary = primary;
    }

    public String getSecondary() {
        if (secondary == null)
            return null;
        if (secondary.length() == 0)
            return null;
        return secondary;
    }

    public void setSecondary(String secondary) {
        this.secondary = secondary;
    }

    public String getTertiary() {
        if (tertiary == null)
            return null;
        if (tertiary.length() == 0)
            return null;
        return tertiary;
    }

    public void setTertiary(String tertiary) {
        this.tertiary = tertiary;
    }

    public String getColor() {
        return color;
    }

    public String getIcon() {
        return icon;
    }
}
