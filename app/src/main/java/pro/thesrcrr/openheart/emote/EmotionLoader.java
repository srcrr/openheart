package pro.thesrcrr.openheart.emote;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.opencsv.CSVReaderHeaderAware;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmotionLoader {

    private final String TAG = "OpenHeart";

    private class Columns {
        public final static String
                ID = "id",
                PRIMARY = "primary",
                SECONDARY = "secondary",
                TERTIARY = "tertiary",
                ICON = "icon",
                COLOR = "color"
                    ;
    }

    private Context context;

    private HashMap<String, PrimaryEmotion> primaryEmotions;
    private HashMap<String, SecondaryEmotion> secondaryEmotions;
    private HashMap<String, TertiaryEmotion> tertiaryEmotions;
    private EmotionList emotions;

    public EmotionLoader(Context context) {
        this.context = context;
        this.emotions = new EmotionList();
        this.primaryEmotions = new HashMap<String, PrimaryEmotion>();
        this.secondaryEmotions = new HashMap<String, SecondaryEmotion>();
        this.tertiaryEmotions= new HashMap<String, TertiaryEmotion>();
    }

    /**
     * Load a list of emtions from a stream.
     * @param in
     * @return A list of emotions.
     */
    public EmotionList fromStream(InputStream in) throws IOException {
        InputStreamReader isr = new InputStreamReader(in);
        List<EmotionVisitor> beans;
        CSVReaderHeaderAware reader = new CSVReaderHeaderAware(isr);

        Map<String, String> map = reader.readMap();
        int id = 0;
        String primary, secondary, tertiary;
        Emotion emotion;
        while (map != null) {
            // Gather the information
            id = Integer.valueOf(map.get(Columns.ID));
            primary = map.get(Columns.PRIMARY);
            secondary = map.get(Columns.SECONDARY);
            tertiary = map.get(Columns.TERTIARY);

            // nullify any empty string
            primary = primary.isEmpty() ? null : primary;
            secondary = secondary.isEmpty() ? null : secondary;
            tertiary = tertiary.isEmpty() ? null : tertiary;

//            System.err.println(
//                    "primary: '" + primary + "', " +
//                    "secondary: '" + secondary + "', " +
//                    "tertiary: '" + tertiary + "'"
//            );

            // Now do the actual storage
            // TODO: store icons and colors
            // Primary emotions
            if (tertiary != null) {
//                System.err.println("  Storing tertiary emotion " + tertiary);
                tertiaryEmotions.put(tertiary,
                        new TertiaryEmotion(
                                id,
                                tertiary,
                                secondaryEmotions.get(secondary)
                        )
                );
                emotions.add(tertiaryEmotions.get(tertiary));
            } else if (secondary != null) {
//                System.err.println("Storing secondary emotion " + secondary);
                secondaryEmotions.put(secondary,
                        new SecondaryEmotion(
                                id,
                                secondary,
                                primaryEmotions.get(primary)
                        )
                );
                emotions.add(secondaryEmotions.get(secondary));
            } else if (primary != null) {
//                System.err.println("Storing primary emotion " + primary);
                primaryEmotions.put(primary,
                        new PrimaryEmotion(
                                id,
                                primary
                        )
                );
                emotions.add(primaryEmotions.get(primary));
            } else {
                Log.e(TAG, "Something went wrong");
                Log.e(TAG, "Primary: " + primary);
                Log.e(TAG, "Secondary: " + secondary);
                Log.e(TAG, "Tertiary: " + tertiary);
                Log.e(TAG, "Map: " + map);
            }
            primary = null;
            secondary = null;
            tertiary = null;
            map = reader.readMap();
        }
        return emotions;
    }

    public EmotionList fromResource(int resourceID) throws IOException {
        Context c = this.context;
        Resources r = c.getResources();
        InputStream in = r.openRawResource(resourceID);
        return this.fromStream(in);
    }
}
