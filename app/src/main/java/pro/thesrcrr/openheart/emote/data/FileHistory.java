package pro.thesrcrr.openheart.emote.data;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.MutableLiveData;

import org.apache.commons.collections.set.ListOrderedSet;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import pro.thesrcrr.openheart.R;

public class FileHistory extends ListOrderedSet {
    private MutableLiveData<ArrayList<String>> fileHistory;
    private final static int RESOURCE = R.raw.file_history;
    private static FileHistory instance = new FileHistory();
    private Context context = null;

    private FileHistory() {}

    public static FileHistory getInstance(Context context) {
        instance.context = context;
        Resources r = instance.context.getResources();
        String pathList = null;
        String [] paths;
        try(InputStream inputStream = r.openRawResource(RESOURCE)) {
            pathList = IOUtils.toString(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        paths = pathList.split("[\n\r]");
        for (String path : paths) {
            instance.add(path);
        }
        return instance;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void insert(String path) {
        this.removeIf(o -> path.equals(o));
        this.add(0, path);
    }
}
