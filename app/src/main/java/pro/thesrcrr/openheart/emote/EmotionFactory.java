package pro.thesrcrr.openheart.emote;

import org.json.JSONException;
import org.json.JSONObject;

class EmotionFactory {
    final private static String CLASS = "class";
    public static Emotion fromJson(JSONObject emotionObject, OHLoadEnvironment env) throws JSONException, JsonColorValueException, JsonColorMissingValueException {
        String className = emotionObject.getString(CLASS);
        if (PrimaryEmotion.class.getName().equals(className)) {
            return PrimaryEmotion.fromJson(emotionObject, env);
        } else if (SecondaryEmotion.class.getName().equals(className)) {
            return SecondaryEmotion.fromJson(emotionObject, env);
        }
        return TertiaryEmotion.fromJson(emotionObject, env);
    }
}
