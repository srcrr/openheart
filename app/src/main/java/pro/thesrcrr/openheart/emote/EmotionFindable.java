package pro.thesrcrr.openheart.emote;

public interface EmotionFindable {
    public Emotion findChildEmotionByName(String name);
}
