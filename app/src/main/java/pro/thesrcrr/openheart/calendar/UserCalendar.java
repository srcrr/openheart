package pro.thesrcrr.openheart.calendar;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Calendars;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import org.apache.commons.collections4.list.TreeList;

import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class UserCalendar {
    private static final String TAG = "UserCalendar";
    private String name;
    private String ownerName;
    private String accountName;
    private String displayName;
    private long id;
    private Context context;

    public static final String ACCOUNT_NAME = "SrcRr-Calendar";
    public static final String ACCOUNT_TYPE = "pro.srcrr.account.localcalendar";

    public static final Account ACCOUNT = new Account(ACCOUNT_NAME, ACCOUNT_TYPE);

    private static final int MY_PERMISSION = 2;
    private final static String [] permissions = {
            Manifest.permission.READ_CALENDAR,
            Manifest.permission.WRITE_CALENDAR,
    };

    public static final String[] CALENDAR_PROJECTION = new String[] {
            Calendars._ID,                           // 0
            Calendars.ACCOUNT_NAME,                  // 1
            Calendars.NAME,                          // 2
            Calendars.CALENDAR_DISPLAY_NAME,         // 3
            Calendars.OWNER_ACCOUNT                  // 4
    };

    // The indices for the projection array above.
    private static final int PROJECTION_ID_INDEX = 0,
                             PROJECTION_ACCOUNT_NAME_INDEX = 1,
                             PROJECTION_NAME_INDEX = 2,
                             PROJECTION_DISPLAY_NAME_INDEX = 3,
                             PROJECTION_OWNER_ACCOUNT_INDEX = 4;

    private UserCalendar(Context context) {
        // Run query
        this.context = context;
    }

    private UserCalendar(Context context, long calID, String name, String displayName, String accountName, String ownerName) {
        this.context = context;
        this.id = calID;
        this.name = name;
        this.displayName = displayName;
        this.accountName = accountName;
        this.ownerName = ownerName;
    }

    public static TreeList<UserCalendar> getAll(Context context) {
        Cursor cur = null;
        ContentResolver cr = context.getContentResolver();
        Uri uri = CalendarContract.Calendars.CONTENT_URI;
        String selection = "((" + Calendars.ACCOUNT_NAME + " = ?) AND ("
                + Calendars.ACCOUNT_TYPE + " = ?) AND ("
                + Calendars.OWNER_ACCOUNT + " = ?))";
        String[] selectionArgs = new String[] {"*", "*", "*"};
        cur = cr.query(uri, CALENDAR_PROJECTION, null, null, null);
        UserCalendar userCalendar;
        TreeList<UserCalendar> calendars = new TreeList<>();
        Log.d(TAG, "getAll: " + selection);
        Log.d(TAG, "getAll: " + selectionArgs);
        Log.d(TAG, CalendarContract.Events.CONTENT_URI.toString());
        while (cur.moveToNext()) {
            long calID = 0;
            String name = null;
            String displayName = null;
            String accountName = null;
            String ownerName = null;

            // Get the field values
            calID = cur.getLong(PROJECTION_ID_INDEX);
            name = cur.getString(PROJECTION_NAME_INDEX);
            displayName = cur.getString(PROJECTION_DISPLAY_NAME_INDEX);
            accountName = cur.getString(PROJECTION_ACCOUNT_NAME_INDEX);
            ownerName = cur.getString(PROJECTION_OWNER_ACCOUNT_INDEX);

//            System.err.println("getAll: calID=" + calID);

            userCalendar = new UserCalendar(context, calID, name, displayName, accountName, ownerName);
            calendars.add(userCalendar);
        }

        cur.close();

        return calendars;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public TreeList<UserCalendarEvent> getEvents() {
        return UserCalendarEvent.getForCalendar(this);
    }

    public UserCalendarEvent findEventById(long id) {
        for (UserCalendarEvent e : getEvents())
            if (e.getId() == id)
                return e;
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public TreeList<UserCalendarEvent> eventsAfter(Date date) {
        TreeList<UserCalendarEvent> events = new TreeList<>();
        for (UserCalendarEvent e : getEvents())
            if (e.getStartTime().after(date))
                events.add(e);
        return events;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public TreeList<UserCalendarEvent> eventsBefore(Date date) {
        TreeList<UserCalendarEvent> events = new TreeList<>();
        for (UserCalendarEvent e : getEvents())
            if (e.getStartTime().before(date))
                events.add(e);
        return events;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public TreeList<UserCalendarEvent> eventsBetween(Date start, Date end) {
        TreeList<UserCalendarEvent> events = new TreeList<>();
        for (UserCalendarEvent e : getEvents())
            if (e.getStartTime().after(start) && e.getEndTime().before(end))
                events.add(e);
        return events;
    }

    static Uri asSyncAdapter(Uri uri, String account, String displayName,
                             String ownerName) {
        return uri.buildUpon()
                .appendQueryParameter(android.provider.CalendarContract.CALLER_IS_SYNCADAPTER,"true")
                .appendQueryParameter(Calendars.CALENDAR_DISPLAY_NAME, displayName)
                .appendQueryParameter(Calendars.ACCOUNT_NAME, account)
                .appendQueryParameter(Calendars.OWNER_ACCOUNT, ownerName)
                .build();
    }

    private static boolean addAccount(Context context) {
        Log.d(TAG, "Adding account...");

        AccountManager am = AccountManager.get(context);
        if (am.addAccountExplicitly(ACCOUNT, null, null)) {
            // explicitly disable sync
            ContentResolver.setSyncAutomatically(ACCOUNT, CalendarContract.AUTHORITY, false);
            ContentResolver.setIsSyncable(ACCOUNT, CalendarContract.AUTHORITY, 0);

            return true;
        } else {
            return false;
        }
    }

    public static UserCalendar create(Context context,
                                      String name,
                                      String displayName,
                                      String accountName,
                                      String ownerName
                                      ) {

//        if (addAccount(context)) {
//            Log.d(TAG, "Account was added!");
//
//            // wait until account is added asynchronously
//            try {
//                Thread.sleep(2000);
//                Log.d(TAG, "after wait...");
//            } catch (InterruptedException e) {
//                Log.e(TAG, "InterruptedException", e);
//            }
//        } else {
//            Log.e(TAG, "There was a problem when trying to add the account!");
//            return null;
//        }

        ContentResolver cr = context.getContentResolver();

        ContentValues cv = new ContentValues();
        cv.put(Calendars.CALENDAR_DISPLAY_NAME, displayName);
        cv.put(Calendars.NAME, name);
//        cv.put(Calendars.ACCOUNT_NAME, accountName);
//        cv.put(Calendars.OWNER_ACCOUNT, ownerName);

        Uri uri = cr.insert(Calendars.CONTENT_URI, cv);

        long id = ContentUris.parseId(uri);

        // FIX: https://stackoverflow.com/a/36093212
        // Work-around for when the calendar is deleted later. It will complain about
        // `name` being null. Set the `ACCOUNT_NAME` and `ACCOUNT_TYPE` to something non-null
//        ContentValues contentValues = new ContentValues();
//        contentValues.put(Calendars.ACCOUNT_NAME, ACCOUNT_NAME);
//        contentValues.put(Calendars.ACCOUNT_TYPE, ACCOUNT_TYPE);
//        String where = new StringBuilder()
//                .append("(")
//                .append(Calendars._ID)
//                .append(" = ")
//                .append(id)
//                .append(")").toString();
//        String[] selectionArgs = { String.valueOf(id) };
//        cr.update(Calendars.CONTENT_URI, contentValues, where, selectionArgs);

        return new UserCalendar(
                context,
                id,
                name,
                displayName,
                accountName,
                ownerName
        );
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public UserCalendarEvent createEvent(String title, String location, boolean isAllDay,
                                         long startTime, long endTime, boolean isOrganizer,
                                         int status, TimeZone timezone) {
        ContentResolver cr = this.context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.TITLE, title);
        values.put(CalendarContract.Events.EVENT_LOCATION, location);
        values.put(CalendarContract.Events.ALL_DAY, isAllDay ? 1 : 0);
        values.put(CalendarContract.Events.DTSTART, startTime);
        if (!isAllDay) {
            values.put(CalendarContract.Events.DTEND, endTime);
        } else {
            long end = Instant.ofEpochMilli(startTime).plusSeconds(1 * 60 * 60).toEpochMilli();
            values.put(CalendarContract.Events.DTEND, end);
        }
        values.put(CalendarContract.Events.IS_ORGANIZER, isOrganizer);
        values.put(CalendarContract.Events.STATUS, status);
        values.put(CalendarContract.Events.CALENDAR_ID, getId());
        values.put(CalendarContract.Events.EVENT_TIMEZONE, timezone.toString());

        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

        long id = ContentUris.parseId(uri);

        return new UserCalendarEvent(id, title, location, isAllDay, startTime,
                                     endTime, isOrganizer, status, this);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public UserCalendarEvent createEvent(String title, String location, long startTime,
                                         long endTime, boolean isOrganizer, int status) {
        TimeZone defaultTz = Calendar.getInstance().getTimeZone();
        return createEvent(title, location, false, startTime, endTime,
                           isOrganizer, status, defaultTz);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public UserCalendarEvent createEvent(String title, String location,
                                         long startTime,
                                         boolean isOrganizer,
                                         int status) {
        TimeZone defaultTz = Calendar.getInstance().getTimeZone();
        return createEvent(title, location, true, startTime, 0,
                           isOrganizer, status, defaultTz);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public UserCalendarEvent createEvent(String title, String location, long startTime,
                                         long endTime, boolean isOrganizer) {
        TimeZone defaultTz = Calendar.getInstance().getTimeZone();
        return createEvent(title, location, false, startTime, endTime,
                           isOrganizer, CalendarContract.Events.STATUS_TENTATIVE,
                defaultTz);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public UserCalendarEvent createEvent(String title, String location,
                                         long startTime,
                                         boolean isOrganizer) {
        TimeZone defaultTz = Calendar.getInstance().getTimeZone();
        return createEvent(title, location, true, startTime, 0,
                           isOrganizer, CalendarContract.Events.STATUS_TENTATIVE,
                defaultTz);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public UserCalendarEvent createEvent(String title, String location, long startTime,
                                         long endTime) {

        TimeZone defaultTz = Calendar.getInstance().getTimeZone();
        return createEvent(title, location, false, startTime, endTime,
                           true, CalendarContract.Events.STATUS_TENTATIVE,
                defaultTz);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public UserCalendarEvent createEvent(String title, String location,
                                         long startTime) {

        TimeZone defaultTz = Calendar.getInstance().getTimeZone();
        return createEvent(title, location, true, startTime, 0,
                           true, CalendarContract.Events.STATUS_TENTATIVE,
                defaultTz);
    }


    public static void requestReadPermissions (Activity activity) {

        ActivityCompat.requestPermissions(activity, permissions, MY_PERMISSION);

        for (String p : permissions) {
            if (ContextCompat.checkSelfPermission(activity, p)
                    != PackageManager.PERMISSION_GRANTED) {
                Log.w(TAG, "requestPermissions: Permission " + p + " not granted" );
            }
        }
    }

    public static Uri getCalUri() {
        return CalendarContract.Calendars.CONTENT_URI.buildUpon()
                .appendQueryParameter(CalendarContract.CALLER_IS_SYNCADAPTER, "false")
                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_NAME, ACCOUNT_NAME)
                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_TYPE, ACCOUNT_TYPE)
                .build();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void delete() {
        ContentResolver cr = context.getContentResolver();

        String where = new StringBuilder().append("((")
//                                          .append(Calendars._ID)
//                                          .append(" = ?) AND (")
                                          .append(Calendars.NAME)
                                          .append(" = ?))")
                                          .toString();
        String[] selectionArgs = { /*String.valueOf(id),*/ name };

        Log.d(TAG, "delete: " + where);
        Log.d(TAG, "delete: SelectionArgs: " + String.join(", ", selectionArgs));

        Uri calUri = ContentUris.withAppendedId(getCalUri(), id);

        cr.delete(calUri, null, null);

//        cr.delete(Calendars.CONTENT_URI, where, selectionArgs);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void clear() {
        for (UserCalendarEvent event : getEvents()) {
            event.delete(context);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
