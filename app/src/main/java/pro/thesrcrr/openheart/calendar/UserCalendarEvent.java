package pro.thesrcrr.openheart.calendar;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import org.apache.commons.collections4.list.TreeList;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

public class UserCalendarEvent {

    private static final String TAG = "UserCalendarEvent";
    private final long id;
    private Calendar calendar;
    private String title;
    private String location;
    private boolean isAllDay;
    private long startTime;
    private long endTime;
    private boolean isOrganizer;
    private int status;
    private UserCalendar userCalendar;

    public static final String[] EVENT_PROJECTION = new String[]{
            Events._ID,
            Events.TITLE,
            Events.EVENT_LOCATION,
            Events.ALL_DAY,
            Events.DTSTART,
            Events.DTEND,
            Events.IS_ORGANIZER,
            Events.STATUS
    };

    // Position references for projection.
    public static final int INDEX_EVENT_ID = 0,
                            INDEX_EVENT_TITLE = 1,
                            INDEX_EVENT_LOCATION = 2,
                            INDEX_EVENT_ALL_DAY = 3,
                            INDEX_EVENT_DTSTART = 4,
                            INDEX_EVENT_DTEND = 5,
                            INDEX_EVENT_IS_ORGANIZER = 6,
                            INDEX_EVENT_STATUS = 7;

    // Json Keys
    public static final String KEY_CALENDAR_ID = "calendar_id",
                               KEY_EVENT_ID = "event_id";


    public UserCalendarEvent(long id,
                             String title, String location, boolean isAllDay, long startTime,
                             long endTime, boolean isOrganizer, int status,
                             UserCalendar userCalendar) {
        this.id = id;
        this.title = title;
        this.location = location;
        this.isAllDay = isAllDay;
        this.startTime = startTime;
        this.endTime = endTime;
        this.isOrganizer = isOrganizer;
        this.status = status;
        this.userCalendar = userCalendar;

        calendar = Calendar.getInstance();
    }

    public static UserCalendarEvent fromCursor(UserCalendar userCalendar, Cursor data) {
        return new UserCalendarEvent(
                data.getLong(INDEX_EVENT_ID),
                data.getString(INDEX_EVENT_TITLE),
                data.getString(INDEX_EVENT_LOCATION),
                data.getInt(INDEX_EVENT_ALL_DAY) == 1,
                data.getLong(INDEX_EVENT_DTSTART),
                data.getLong(INDEX_EVENT_DTEND),
                data.getInt(INDEX_EVENT_IS_ORGANIZER) == 1,
                data.getInt(INDEX_EVENT_STATUS),
                userCalendar
                );
    }

    /**
     * Get a UserCalendarEvent.
     * @param calendars List of user calendars
     * @param jsonObject JSON object to parse.
     * @return a new UserCalendarEvent from the jsonObject.
     * @throws JSONException if a json exception occurs.
     */
    public static UserCalendarEvent fromJson(TreeList<UserCalendar> calendars,
                                             JSONObject jsonObject) throws JSONException {
        long calendarId = jsonObject.getLong(KEY_CALENDAR_ID),
                eventId = jsonObject.getLong(KEY_EVENT_ID);
        UserCalendar calendar = null;
        for (UserCalendar cal : calendars)
            if (cal.getId() == calendarId)
                calendar = cal;
        if (calendar == null)
            return null;
        return calendar.findEventById(eventId);
    }

    public long getId() {
        return id;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isAllDay() {
        return isAllDay;
    }

    public void setAllDay(boolean allDay) {
        isAllDay = allDay;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Date getStartTime() {
        return Date.from(Instant.ofEpochMilli(startTime));
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public Date getEndTime() {
        return Date.from(Instant.ofEpochMilli(endTime));
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public boolean isOrganizer() {
        return isOrganizer;
    }

    public void setOrganizer(boolean organizer) {
        isOrganizer = organizer;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public UserCalendar getUserCalendar() {
        return userCalendar;
    }

    public void setUserCalendar(UserCalendar userCalendar) {
        this.userCalendar = userCalendar;
    }

    public static TreeList<UserCalendarEvent> getForCalendar(UserCalendar userCalendar,
                                                             Date start,
                                                             Date end) {
        long startMillis = 0, endMillis = 0;
        Cursor cur = null;
        TreeList<String> selectionArgs = new TreeList<>();
        TreeList<UserCalendarEvent> events = new TreeList<>();
        ContentResolver cr = userCalendar.getContext().getContentResolver();
        Uri uri = CalendarContract.Events.CONTENT_URI;

        // start building the selectionArgs
        selectionArgs.add(String.valueOf(userCalendar.getId()));

        // Build the query.
        // Include the start time and end time in the query if it exists.
        StringBuilder queryBuilder = new StringBuilder("(")
                .append(CalendarContract.Events.CALENDAR_ID)
                .append(" = ? )");
        if (start != null) {
            startMillis = start.getTime();
            queryBuilder.append(" AND (").append(Events.DTSTART)
                        .append("> ?)");
            selectionArgs.add(String.valueOf(startMillis));
        }
        if (end != null) {
            endMillis = end.getTime();
            queryBuilder.append(" AND (").append(Events.DTEND)
                        .append("< ?)");
            selectionArgs.add(String.valueOf(endMillis));
        }

        String selection = queryBuilder.toString();

        cur = cr.query(uri, EVENT_PROJECTION, selection, selectionArgs.toArray(new String[]{}),
                null);
        while (cur.moveToNext()) {
            events.add(UserCalendarEvent.fromCursor(userCalendar, cur));
        }

        return events;
    }

    public static TreeList<UserCalendarEvent> getForCalendarFrom(UserCalendar userCalendar,
                                                                 Date start) {
        return getForCalendar(userCalendar, start, null);
    }

    public static TreeList<UserCalendarEvent> getForCalendarTo(UserCalendar userCalendar,
                                                               Date end) {
        return getForCalendar(userCalendar, null, end);
    }

    public static TreeList<UserCalendarEvent> getForCalendar(UserCalendar userCalendar) {
        return getForCalendar(userCalendar, null, null);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static TreeList<UserCalendarEvent> getAllBetween(Context context, Date start, Date end) {
        TreeList<UserCalendar> calendars = UserCalendar.getAll(context);
        TreeList<UserCalendarEvent> events = new TreeList<>();
        for (UserCalendar calendar : calendars) {
            for (UserCalendarEvent event : calendar.getEvents()) {
                if (start == null && end == null)
                    events.add(event);
                else if (start == null && event.getEndTime().before(end))
                    events.add(event);
                else if (end == null && event.getStartTime().after(start))
                    events.add(event);
                else if (end != null && start != null && event.getStartTime().after(start)
                         && event.getEndTime().after(end))
                    events.add(event);
            }
        }
        return events;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static TreeList<UserCalendarEvent> getAllAfter(Context context, Date start) {
        return getAllBetween(context, start, null);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static TreeList<UserCalendarEvent> getAllBefore(Context context, Date end) {
        return getAllBetween(context, null, end);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static TreeList<UserCalendarEvent> getAll(Context context) {
        return getAllBetween(context, null, null);
    }

    public JSONObject toJsonReference() throws JSONException {
        JSONObject obj = new JSONObject();
        return obj.put(KEY_CALENDAR_ID, userCalendar.getId())
                  .put(KEY_EVENT_ID, getId());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void delete(Context context) {
        ContentResolver cr = context.getContentResolver();

//        Log.d(TAG, "delete: EVENT `" + getTitle() + "`");

        String where = new StringBuilder().append("((")
                                          .append(Events._ID)
                                          .append(" = ?) AND (")
                .append(Events.CALENDAR_ID)
                .append(" = ?))")
                .toString();
        String[] selectionArgs = { String.valueOf(id), String.valueOf(userCalendar.getId()) };

//        Log.d(TAG, "delete: " + where);
//        Log.d(TAG, "delete: SelectionArgs: " + String.join(", ", selectionArgs));

        cr.delete(Events.CONTENT_URI, where, selectionArgs);
    }

    @NonNull
    @Override
    public String toString() {
        return (
                new StringBuilder("<UserCalendarEvent (id=")
            ).append(id)
                .append(", title=\"")
                .append(title)
                .append("\", location=\"")
                .append(location)
                .append("\", startTime=<")
                .append((new Date(startTime)).toString())
                .append(">, endTime=<")
                .append((new Date(endTime)).toString())
                .append(">)>")
                .toString();
    }
}
