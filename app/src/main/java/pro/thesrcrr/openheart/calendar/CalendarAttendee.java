package pro.thesrcrr.openheart.calendar;

import android.database.Cursor;
import android.provider.CalendarContract;

public class CalendarAttendee {

    public static final String[] PROJECTION = new String[] {
            CalendarContract.Attendees._ID,
            CalendarContract.Attendees.EVENT_ID,
            CalendarContract.Attendees.ATTENDEE_EMAIL,
            CalendarContract.Attendees.ATTENDEE_RELATIONSHIP,
            CalendarContract.Attendees.ATTENDEE_TYPE,
            CalendarContract.Attendees.ATTENDEE_STATUS,
    };

    public final static int INDEX_ID = 0,
                            INDEX_EVENT_ID = 1,
                            INDEX_ATTENDEE_NAME = 2,
                            INDEX_ATTENDEE_RELATIONSHIP = 3,
                            INDEX_ATTENDEE_TYPE = 4,
                            INDEX_ATTENDEE_STATUS = 5
                            ;
    private UserCalendar calendar;
    private UserCalendarEvent event;
    private String name;
    private int relationship;
    private int type;
    private int status;

    public CalendarAttendee(UserCalendar calendar, UserCalendarEvent event, String name, int relationship, int type, int status) {
        this.calendar = calendar;
        this.event = event;
        this.name = name;
        this.relationship = relationship;
        this.type = type;
        this.status = status;
    }

    public CalendarAttendee fromCursor(UserCalendar calendar, UserCalendarEvent event,
                                       Cursor cursor) {
        CalendarAttendee participant = new CalendarAttendee(
                calendar, event,
                cursor.getString(INDEX_ATTENDEE_NAME),
                cursor.getInt(INDEX_ATTENDEE_RELATIONSHIP),
                cursor.getInt(INDEX_ATTENDEE_TYPE),
                cursor.getInt(INDEX_ATTENDEE_STATUS)
        );

        return participant;
    }

    public UserCalendar getCalendar() {
        return calendar;
    }

    public void setCalendar(UserCalendar calendar) {
        this.calendar = calendar;
    }

    public UserCalendarEvent getEvent() {
        return event;
    }

    public void setEvent(UserCalendarEvent event) {
        this.event = event;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRelationship() {
        return relationship;
    }

    public void setRelationship(int relationship) {
        this.relationship = relationship;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
