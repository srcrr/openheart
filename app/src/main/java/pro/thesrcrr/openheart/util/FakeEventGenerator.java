package pro.thesrcrr.openheart.util;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.CalendarContract;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.github.javafaker.Faker;

import org.apache.commons.collections4.list.TreeList;

import java.util.Calendar;
import java.util.Date;

import pro.thesrcrr.openheart.calendar.UserCalendar;
import pro.thesrcrr.openheart.calendar.UserCalendarEvent;

public class FakeEventGenerator {
    private static final String TAG = "FakeEventGenerator";
    private ContentResolver contentResolver;
    private final Uri uri;
    public TreeList<UserCalendarEvent> generated = new TreeList<>();
    private Context context = null;
    private final Faker faker = new Faker();

    public FakeEventGenerator(Context context) {
        this.context = context;
        uri = CalendarContract.Events.CONTENT_URI;
        contentResolver = context.getContentResolver();
    }

    private String fakeTitle() {
        return faker.lorem().sentence();
    }

    private long getLastId(String title, String location, long calendarId) {
        ContentResolver cr = context.getContentResolver();

        String[] projection = { CalendarContract.Events._ID };
        String selection = new StringBuilder("(").append(CalendarContract.Events.EVENT_LOCATION)
                                                 .append(" = ?) AND (")
                                                 .append(CalendarContract.Events.TITLE)
                                                 .append(" = ?) AND (")
                                                 .append(CalendarContract.Events.CALENDAR_ID)
                                                 .append(" = ?)").toString();
        String[] selectionArgs = {title, location, String.valueOf(calendarId)};
        Cursor cur = cr.query(CalendarContract.Events.CONTENT_URI,
                              projection, selection, selectionArgs, null);
        return cur.moveToNext() ? cur.getLong(0) : -1;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void generate(UserCalendar calendar, Date start, int intervals) {

        String timezone = Calendar.getInstance().getTimeZone().toZoneId().toString();

        contentResolver = context.getContentResolver();

        ContentValues values = new ContentValues();
        int allDay = faker.number().numberBetween(0, 2),
            isOrganizer = faker.number().numberBetween(0, 2);
        Date end = null;
        String title = fakeTitle();
        String location = faker.lordOfTheRings().location();
        values.put(CalendarContract.Events.TITLE, title);
        values.put(CalendarContract.Events.EVENT_LOCATION, location);
        values.put(CalendarContract.Events.ALL_DAY, allDay);
        if (allDay == 0) {
            values.put(CalendarContract.Events.DTSTART, start.getTime());
            long endLong = (long) (intervals * 15 * 60);
            end = Date.from(start.toInstant().plusSeconds(endLong));
            values.put(CalendarContract.Events.DTEND, end.getTime());
        }
        values.put(CalendarContract.Events.IS_ORGANIZER, isOrganizer);
        values.put(CalendarContract.Events.CALENDAR_ID, calendar.getId());
        values.put(CalendarContract.Events.EVENT_TIMEZONE, timezone);
        values.put(CalendarContract.Events.EVENT_END_TIMEZONE, timezone);

        Log.d(TAG, "generate: Inserting into " + uri.toString());
        Log.d(TAG, "generate: values = " + values.toString());

//        contentResolver = context.getContentResolver();
        Uri inserted = contentResolver.insert(CalendarContract.Events.CONTENT_URI, values);

//        long lastInserted = ContentUris.parseId(inserted);
        // Work-around. TODO: find out why lastInserted is null!
        long lastInserted = getLastId(title, location, calendar.getId());

        generated.add(new UserCalendarEvent(
                lastInserted,
                title,
                location,
                allDay == 1,
                (start == null ? 0 : start.getTime()),
                (end == null ? 0 : end.getTime()),
                isOrganizer == 1,
                CalendarContract.Events.STATUS_TENTATIVE,
                calendar
        ));
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void generate(UserCalendar calendar, Date start) {
        generate(calendar, start, faker.number().numberBetween(1, 5));
    }

    public void cleanUp(UserCalendar calendar) {
        StringBuilder where;
        String [] whereArgs = new String[2];
        for (UserCalendarEvent event : generated) {
            Log.d(TAG, "cleanUp: " + event.getTitle());
            where = new StringBuilder("(").append(CalendarContract.Events._ID)
                                          .append(" = ?) AND (")
                                          .append(CalendarContract.Events.CALENDAR_ID)
                                          .append(" = ?");
            whereArgs [1] = String.valueOf(event.getId());
            whereArgs [2] = String.valueOf(calendar.getId());
            contentResolver.delete(CalendarContract.Events.CONTENT_URI,
                                   where.toString(),
                                   whereArgs);
        }
    }
}
