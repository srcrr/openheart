package pro.thesrcrr.openheart.util;

public class Hit<T> {

    private T item;
    private int count;

    public Hit(T item) {
        this.item = item;
        this.count = 1;
    }

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
