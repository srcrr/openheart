package pro.thesrcrr.openheart.util;

import org.json.JSONException;
import org.json.JSONObject;

public interface Jsonable {
    JSONObject toJson() throws JSONException;
    String NEWLINE = "\n", STRING_DELIM="\"";
}
