package pro.thesrcrr.openheart.util;

import org.apache.commons.collections4.SortedBag;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class HitList<T> implements SortedBag<T> {
    private Map<T, Integer> counts = new HashMap<T, Integer>();

    public Map<T, Integer> getCounts() {
        return counts;
    }

    public int get(T t) {
        if (counts.containsKey(t))
            return counts.get(t).intValue();
        return 0;
    }

    @Override
    public int getCount(Object object) {
        return 0;
    }

    public boolean add(T t, int n) {
        if (counts.get(t) == null) {
            counts.put(t, n);
        } else {
            counts.put(t, counts.get(t) + n);
        }
        return true;
    }

    @Override
    public boolean add(Object t) {
        return this.add((T) t, 1);
    }

    public boolean remove(Object o, int n) {
        if (!(o != null && counts.get(o) != null))
            return false;
        if (counts.get(o) > 0){
            counts.put((T) o, counts.get(o) - Math.min(n, counts.get(o)));
            return false;
        }
        if (counts.get(o) == 1) {
            counts.remove(o);
            return true;
        }
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return remove(o, 1);
    }

    @Override
    public Set<T> uniqueSet() {
        return counts.keySet();
    }

    @Override
    public int size() {
        return counts.keySet().size();
    }

    @Override
    public boolean isEmpty() {
        return counts.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return counts.containsKey(o);
    }

    @Override
    public boolean containsAll(Collection<?> coll) {
        return counts.keySet().contains(coll);
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T o : c)
            add(c);
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> coll) {
        for (Object o : coll)
            remove(o);
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> coll) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean equals(Object o) {
        return (false);
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public Iterator<T> iterator() {
        return counts.keySet().iterator();
    }

    @Override
    public Object[] toArray() {
        return counts.keySet().toArray();
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return (T1[]) counts.keySet().toArray();
    }

    public List<T> top(int n) {
        List<T> items = new LinkedList<>();
        T [] arr = (T[]) toArray();
        for (int i = n; i > 0; ++i) {
            items.add(arr[i]);
        }
        return items;
    }

    @Override
    public Comparator<? super T> comparator() {
        return (Comparator<T>) (o1, o2) -> counts.get(o1) - counts.get(o2);
    }

    @Override
    public T first() {
        T f = null;
        for (T i : counts.keySet()) {
            if (f == null || counts.get(i) < counts.get(f))
                f = i;
        }
        return f;
    }

    @Override
    public T last() {
        T l = null;
        for (T i : counts.keySet()) {
            if (l == null || counts.get(i) > counts.get(l))
                l = i;
        }
        return l;
    }
}
