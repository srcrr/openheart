package pro.thesrcrr.openheart.util;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.preference.Preference;
import androidx.preference.PreferenceManager;

import java.util.Set;

public class PrefGet {

    public static String getString(Context context, int resource, String defValue) {
        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context /* Activity context */);
        return sharedPreferences.getString(context.getResources().getString(resource), defValue);
    }

    public static float getFloat(Context context, int resource, float defValue) {
        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context/* Activity context */);
        try {
            return sharedPreferences.getFloat(context.getResources().getString(resource), defValue);
        } catch (ClassCastException e) {
            String s = sharedPreferences.getString(context.getResources().getString(resource),
                                                   String.valueOf(defValue));
            return Float.valueOf(s).floatValue();
        }
    }

    public static Set<String> getStringSet(Context context, int resource, Set<String> defValue) {
        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(context/* Activity context */);
        return sharedPreferences.getStringSet(context.getResources().getString(resource), defValue);
    }

    public static int [] getIntArray(Context context, int resource) {
        Set<String> val = getStringSet(context, resource, null);
        if (val == null) {
            return null;
        }
        int [] ret = new int[val.size()];
        for (int i = 0; i < ret.length; ++i) {
            ret[i] = Integer.valueOf((String) val.toArray()[i]).intValue();
        }
        return ret;
    }

    public static boolean arrayHasValue(Context context, int resource, int value) {
        Set<String> val = getStringSet(context, resource, null);
        if (val == null)
            return false;
        for (String v : val)
            if (String.valueOf(value).equals(v))
                return true;
        return false;
    }

    public static boolean is(Preference preference, int keyResource) {
        return preference.hasKey() && preference.getKey().equals(
                preference.getContext().getResources().getString(keyResource)
        );
    }
}
