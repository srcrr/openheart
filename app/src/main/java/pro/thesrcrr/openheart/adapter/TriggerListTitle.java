package pro.thesrcrr.openheart.adapter;

import pro.thesrcrr.openheart.emote.Emotion;

public class TriggerListTitle extends TriggerListItem {

    public TriggerListTitle(Emotion e) {
        super(e);
    }
}
