package pro.thesrcrr.openheart.adapter;

import pro.thesrcrr.openheart.emote.Emotion;
import pro.thesrcrr.openheart.trigger.TriggerMetric;

public class TriggerListStep extends TriggerListItem {
    private final TriggerMetric triggerMetric;

    public TriggerListStep(Emotion e, TriggerMetric t) {
        super(e);
        this.triggerMetric = t;
    }

    public TriggerMetric getTriggerMetric() {
        return triggerMetric;
    }
}
