package pro.thesrcrr.openheart.adapter;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.List;

import pro.thesrcrr.openheart.MainActivity;
import pro.thesrcrr.openheart.emote.Emotion;
import pro.thesrcrr.openheart.trigger.TriggerMetric;
import pro.thesrcrr.openheart.util.HitList;

public class TriggerListContent {
    private final MainActivity mainActivity;

    private List<TriggerListItem> triggerListItems = new ArrayList<>();

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public TriggerListContent(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public List<TriggerListItem> getTriggerListItems() {
        if (mainActivity.getMoodplot() == null)
            return triggerListItems;
        if (mainActivity.getMoodplot().getCounts() == null)
            return triggerListItems;
        HitList<Emotion> emotions = mainActivity.getMoodplot().getCounts();
        for (Emotion e : emotions) {
            triggerListItems.add(new TriggerListTitle(e));
            for (TriggerMetric t : mainActivity.getMoodplot().getMetricsForMood(e)) {
                triggerListItems.add(new TriggerListStep(e, t));
            }
        }
        return triggerListItems;
    }

    public void setTriggerListItems(List<TriggerListItem> triggerListItems) {
        this.triggerListItems = triggerListItems;
    }
}
