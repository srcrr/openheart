package pro.thesrcrr.openheart.adapter;

import pro.thesrcrr.openheart.emote.Emotion;

public class TriggerListItem {
    private final Emotion emotion;

    public TriggerListItem(Emotion e) {
        this.emotion = e;
    }

    public Emotion getEmotion() {
        return emotion;
    }
}
